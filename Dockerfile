FROM golang:1.23 AS build
ARG git_personal_token
ARG VERSION

RUN git config --global url."https://gitlab-ci-token:$git_personal_token@gitlab.com/".insteadof "https://gitlab.com/"
RUN echo "machine gitlab.com login gitlab-ci-token password $git_personal_token" > $HOME/.netrc
RUN go install github.com/go-task/task/v3/cmd/task@latest

WORKDIR /app
COPY . .
RUN BUILD_FLAGS="-X 'gitlab.com/verosint/public/verosint/build.version=${VERSION}'" task build

FROM alpine:3.20 AS app
RUN apk --no-cache --update add ca-certificates
RUN addgroup -S appgroup && adduser -S appuser -G appgroup
USER appuser

LABEL version=$VERSION
COPY --from=build /app/verosint /
ENTRYPOINT ["/verosint"]