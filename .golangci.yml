# Copyright (c) "2025 Verosint, Inc"
run:
  concurrency: 0
  timeout: 15m
  issues-exit-code: 2
  tests: true
  modules-download-mode: readonly
  allow-parallel-runners: true
  go: "1.24"

output:
  print-issued-lines: true
  print-linter-name: true
  path-prefix: ""
  sort-results: false

linters-settings:
  asasalint:
    use-builtin-exclusions: true
    ignore-test: false

  bidichk:
    left-to-right-embedding: true
    right-to-left-embedding: true
    pop-directional-formatting: true
    left-to-right-override: true
    right-to-left-override: true
    left-to-right-isolate: true
    right-to-left-isolate: true
    first-strong-isolate: true
    pop-directional-isolate: true

  copyloopvar:
    check-alias: true

  depguard:
    rules:
      main:
        list-mode: lax
        deny:
          - pkg: "math/rand$"
            desc: use math/rand/v2

  dogsled:
    max-blank-identifiers: 2

  errcheck:
    check-type-assertions: true
    exclude-functions:
      - encoding/json.Marshal
      - encoding/json.MarshalIndent

  errchkjson:
    check-error-free-encoding: true
    report-no-exported: true

  errorlint:
    errorf: true
    errorf-multi: true
    asserts: true
    comparison: true

  exhaustive:
    check:
      - switch
    check-generated: false
    default-signifies-exhaustive: false
    ignore-enum-members: ""
    ignore-enum-types: ""
    package-scope-only: false
    explicit-exhaustive-switch: false
    explicit-exhaustive-map: false

  testifylint:
    # disable by disable: 
    #              - ...
    enable:
      - blank-import
      - bool-compare
      - compares
      - empty
      - error-is-as
      - error-nil
      - expected-actual
      - float-compare
      - go-require
      - len
      - negative-positive
      - nil-compare
      - require-error
      - suite-dont-use-pkg
      - suite-extra-assert-call
      - suite-thelper
      - useless-assert
    bool-compare:
      ignore-custom-types: true
    expected-actual:
      pattern: ^expected
    go-require:
      ignore-http-handlers: true
    require-error:
      fn-pattern: ^(Errorf?|NoErrorf?)$
    suite-extra-assert-call:
      mode: require

  forbidigo:
    forbid:
      - ^print\.*$
      - p: ^fmt\.Print.*$
        msg: Do not commit print statements.
      - p: ^r.*\.StatusCode.*$
        msg: Do not use StatusCode directly, call GetStatusCode() instead.
    exclude-godoc-examples: false
    analyze-types: false

  gocritic:
    # Which checks should be enabled; can't be combined with 'disabled-checks';
    # See https://go-critic.github.io/overview#checks-overview
    # To check which checks are enabled run `GL_DEBUG=gocritic golangci-lint run`
    # By default list of stable checks is used.
    enabled-tags:
      - diagnostic
      - performance
      - style
    disabled-tags:
      - experimental
      - opinionated
    settings:
      captLocal:
        paramsOnly: false
      elseif:
        skipBalanced: false
      hugeParam:
        sizeThreshold: 255
      rangeExprCopy:
        sizeThreshold: 512
        skipTestFuncs: true
      rangeValCopy:
        sizeThreshold: 128
        skipTestFuncs: true
      underef:
        skipRecvDeref: true

  gocyclo:
    min-complexity: 30

  gofumpt:
    extra-rules: false

  goheader:
    template: |-
      Copyright (c) "{{ YEAR }} Verosint, Inc"

  goimports:
    local-prefixes: gitlab.com/verosint

  gomoddirectives:
    replace-local: false
    retract-allow-no-explanation: false
    exclude-forbidden: false

  gosimple:
    checks: [ "all" ]

  gosec:
    includes:
      - G101
      - G102
      - G103
      - G104
      - G106
      - G107
      - G108
      - G109
      - G110
      - G111
      - G112
      - G113
      - G114
      - G115
      - G201
      - G202
      - G203
      - G204
      - G301
      - G302
      - G303
      - G304
      - G305
      - G306
      - G307
      - G401
      - G402
      - G403
      - G404
      - G405
      - G406
      - G501
      - G502
      - G503
      - G504
      - G506
      - G507
      - G601
      - G602
    excludes:
      - G204
      - G505
    exclude-generated: true
    severity: low
    confidence: low
    concurrency: 12
    config:
      G301: "0750"
      G302: "0600"
      G306: "0600"
      G101:
        pattern: "(?i)example"
        ignore_entropy: false
        entropy_threshold: "80.0"
        per_char_threshold: "3.0"
        truncate: "32"

  govet:
    enable-all: true
    disable:
      - fieldalignment
      - shadow

  importas:
    no-unaliased: true
    no-extra-aliases: false
    alias:
      - pkg: github.com/deckarep/golang-set/v2
        alias: mapset
      - pkg: github.com/AppsFlyer/go-sundheit
        alias: gosundheit
      - pkg: github.com/vmware/vmware-go-kcl-v2/clientlibrary/interfaces
        alias: kc
      - pkg: github.com/hashicorp/golang-lru/arc/v2
        alias: lru
      - pkg: github.com/aws/aws-sdk-go-v2/config
        alias: awscfg
      - pkg: github.com/aws/aws-sdk-go-v2/aws/transport/http
        alias: awshttp
      - pkg: github.com/aws/aws-sdk-go-v2/service/kinesis/types
        alias: ktypes
      - pkg: github.com/aws/aws-sdk-go-v2/service/sqs/types
        alias: stypes
      - pkg: github.com/auth0/go-jwt-middleware/v2
        alias: jwtmiddleware
      - pkg: gopkg.in/DataDog/dd-trace-go.v1/contrib/database/sql
        alias: sqltrace
      - pkg: gopkg.in/DataDog/dd-trace-go.v1/contrib/gorm.io/gorm.v1
        alias: gormtrace
      - pkg: gorm.io/gorm/logger
        alias: gormlogger
      - pkg: github.com/AppsFlyer/go-sundheit/http
        alias: healthhttp
      - pkg: github.com/go-chi/chi/v5/middleware
        alias: chimiddleware
      - pkg: gopkg.in/DataDog/dd-trace-go.v1/contrib/net/http
        alias: ddhttp
      - pkg: gopkg.in/DataDog/dd-trace-go.v1/contrib/go-chi/chi.v5
        alias: chitrace
      - pkg: gopkg.in/DataDog/dd-trace-go.v1/contrib/aws/aws-sdk-go-v2/aws
        alias: ddaws

  makezero:
    always: false

  misspell:
    locale: US
    ignore-words:
      - 443ID
      - Verosint

  nakedret:
    max-func-lines: 30

  nilnil:
    checked-types:
      - ptr
      - func
      - iface
      - map
      - chan

  nolintlint:
    allow-unused: false
    require-explanation: true
    require-specific: true

  predeclared:
    q: true

  staticcheck:
    checks: [ "all" ]

  stylecheck:
    checks: [ "all", "-ST1000", "-ST1003", "-ST1016", "-ST1020", "-ST1021", "-ST1022" ]
    dot-import-whitelist:
      - fmt
    initialisms: [ "ACL", "API", "ASCII", "CPU", "CSS", "DNS", "EOF", "GUID", "HTML", "HTTP", "HTTPS", "ID", "IP", "JSON", "QPS", "RAM", "RPC", "SLA", "SMTP", "SQL", "SSH", "TCP", "TLS", "TTL", "UDP", "UI", "GID", "UID", "UUID", "URI", "URL", "UTF8", "VM", "XML", "XMPP", "XSRF", "XSS" ]
    http-status-code-whitelist: [ "200", "400", "404", "500" ]

  testpackage:
    # regexp pattern to skip files
    skip-regexp: _test\.go

  thelper:
    test:
      first: true
      name: true
      begin: true
    benchmark:
      first: true
      name: true
      begin: true
    tb:
      first: true
      name: true
      begin: true
    fuzz:
      first: true
      name: true
      begin: true

  whitespace:
    multi-if: false
    multi-func: false

  usestdlibvars:
    http-method: true
    http-status-code: true
    time-weekday: true
    time-month: true
    time-layout: true
    crypto-hash: true
    default-rpc-path: true
    sql-isolation-level: true
    tls-signature-scheme: true
    constant-kind: true

  unparam:
    check-exported: true

linters:
  enable:
  - asasalint
  - bidichk
  - copyloopvar
  - depguard
  - dogsled
  - errorlint
  - exhaustive
  - fatcontext
  - forbidigo
  - gocritic
  - gocyclo
  - gofumpt
  - goheader
  - goimports
  - gomoddirectives
  - gosimple
  - gosec
  - govet
  - importas
  - makezero
  - misspell
  - nakedret
  - nilnil
  - nolintlint
  - predeclared
  - staticcheck
  - stylecheck
  - testpackage
  - thelper
  - unused
  - unparam
  - usetesting
  - usestdlibvars
  - whitespace
  - testifylint
  disable:
  - errcheck
  - godot
  - tagliatelle
  fast: false


issues:
  exclude-rules:
    - path: handlers/generated/
      linters:
        - goheader
        - gofumpt
        - goimports

    - path: _test\.go
      linters:
        - gocyclo
        - errcheck
        - dupl
        - gosec

    - path: internal/hmac/
      text: "weak cryptographic primitive"
      linters:
        - gosec

    - linters:
        - staticcheck
      text: "SA9003:"

  exclude-use-default: false
  exclude-case-sensitive: false
  include:
    - EXC0002 # disable excluding of issues about comments from golint
  max-issues-per-linter: 0
  max-same-issues: 0
  new: true
  fix: false

severity:
  default-severity: error
  case-sensitive: false
