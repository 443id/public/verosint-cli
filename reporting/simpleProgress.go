// Copyright (c) "2025 Verosint, Inc"
package reporting

import (
	"fmt"
	"io"
	"runtime"
	"time"
)

type ProgressReporter interface {
	Progress()
	Close()
}

type SimpleProgress struct {
	periodStart     time.Time
	intervalSeconds float64
	stats           *StatsReporter
	out             io.Writer
	called          int
}

var progressChars = []string{"∙∙∙", "●∙∙", "∙●∙", "∙∙●"}

func NewSimpleProgress(out io.Writer, stats *StatsReporter) *SimpleProgress {
	cursorOff(out)
	var interval float64
	if isWindows() {
		interval = 5.0
	} else {
		interval = 0.5
	}
	return &SimpleProgress{
		periodStart:     time.Now(),
		intervalSeconds: interval,
		stats:           stats,
		out:             out,
		called:          0,
	}
}

func (s *SimpleProgress) Progress() {
	periodDuration := time.Since(s.periodStart)
	if periodDuration.Seconds() >= s.intervalSeconds {
		s.display(s.stats)
		s.periodStart = time.Now()
	}
}

func (s *SimpleProgress) Close() {
	s.display(s.stats)
	cursorOn(s.out)
}

func (s *SimpleProgress) display(_ *StatsReporter) {
	msg := s.getProgressMessage()

	if isWindows() {
		msg += "\n"
	}

	s.called++
	_, _ = s.out.Write([]byte(msg))
}

func (s *SimpleProgress) getProgressMessage() string {
	if isWindows() {
		return fmt.Sprintf("Processed: %v, Errors: %v",
			s.stats.Processed,
			s.stats.Errors)
	} else {
		return fmt.Sprintf("\x1b[0G\x1b[2K%s Processed: \x1b[1m%v\x1b[22m, Errors: %v",
			progressChars[s.called%len(progressChars)],
			s.stats.Processed,
			s.stats.Errors)
	}
}

func cursorOff(out io.Writer) {
	if !isWindows() {
		_, _ = out.Write([]byte("\x1b[?25l"))
	}
}

func cursorOn(out io.Writer) {
	if !isWindows() {
		_, _ = out.Write([]byte("\x1b[?25h\n"))
	}
}

func isWindows() bool {
	return runtime.GOOS == "windows"
}
