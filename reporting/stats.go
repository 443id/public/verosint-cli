// Copyright (c) "2025 Verosint, Inc"
package reporting

import (
	"encoding/json"
	"io"

	"gitlab.com/verosint/public/verosint/formatters"
)

type StatsReporter struct {
	Processed int `json:"processed"`
	Errors    int `json:"errors"`
}

func NewStatsHandler() *StatsReporter {
	return &StatsReporter{
		Processed: 0,
		Errors:    0,
	}
}

func (s *StatsReporter) Handle(record *formatters.Record) {
	s.Processed++
	if record.Error != "" {
		s.Errors++
	}
}

func (s *StatsReporter) JsonReport(writer io.Writer) error {
	encoder := json.NewEncoder(writer)
	encoder.SetIndent("", "  ")
	return encoder.Encode(s)
}
