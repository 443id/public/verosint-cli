// Copyright (c) "2025 Verosint, Inc"
package reporting

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/verosint/public/verosint/formatters"
)

func TestNewStatsHandler(t *testing.T) {
	tests := []struct {
		name              string
		mockStatsReporter StatsReporter
	}{
		{
			name: "NewStatsHandler: Good Call",
			mockStatsReporter: StatsReporter{
				Processed: 0,
				Errors:    0,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			statsReportTest := NewStatsHandler()
			assert.Equal(t, test.mockStatsReporter.Processed, statsReportTest.Processed)
			assert.Equal(t, test.mockStatsReporter.Errors, statsReportTest.Errors)
		})
	}
}

func TestHandle(t *testing.T) {
	tests := []struct {
		name                  string
		mockStatsReporter     StatsReporter
		expectedStatsReporter StatsReporter
		mockRecord            formatters.Record
	}{
		{
			name: "Handle: Score Result has error",
			mockStatsReporter: StatsReporter{
				Processed: 0,
				Errors:    5,
			},
			expectedStatsReporter: StatsReporter{
				Processed: 1,
				Errors:    6,
			},
			mockRecord: formatters.Record{
				ID: fmt.Sprintf("%v", 0),
				Identifiers: map[string]string{
					"email": "Test String",
					"ip":    "Test String",
					"phone": "Test String",
				},
				Parameters: map[string]interface{}{},
				Version:    "1.0",
				RequestId:  "fakeID",
				Error:      "MOCK ERROR",
			},
		},
		{
			name: "Handle: Good Call",
			mockStatsReporter: StatsReporter{
				Processed: 0,
				Errors:    5,
			},
			expectedStatsReporter: StatsReporter{
				Processed: 1,
				Errors:    5,
			},
			mockRecord: formatters.Record{
				ID: fmt.Sprintf("%v", 0),
				Identifiers: map[string]string{
					"email": "Test String",
					"ip":    "Test String",
					"phone": "Test String",
				},
				Parameters: map[string]interface{}{},
				Version:    "1.0",
				RequestId:  "fakeID",
				Error:      "",
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			test.mockStatsReporter.Handle(&test.mockRecord)
		})
	}
}

func TestJsonReport(t *testing.T) {
	tests := []struct {
		name              string
		mockStatsReporter StatsReporter
		expectedErr       error
	}{
		{
			name: "JsonReport: Good Call",
			mockStatsReporter: StatsReporter{
				Processed: 0,
				Errors:    0,
			},
			expectedErr: nil,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var b bytes.Buffer

			bCopy := b

			err := test.mockStatsReporter.JsonReport(&b)

			assert.Equal(t, err, test.expectedErr)

			assert.NotEqual(t, b, bCopy)
		})
	}
}
