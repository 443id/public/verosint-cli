// Copyright (c) "2025 Verosint, Inc"
package reporting

import (
	"bytes"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewSimpleProgress(t *testing.T) {
	tests := []struct {
		name              string
		FakeStatsReporter StatsReporter
		expectedInterval  float64
	}{
		{
			name:             "NewSimpleProgress: Good Call",
			expectedInterval: 0.5,
			FakeStatsReporter: StatsReporter{
				Processed: 10000,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actualOut := new(bytes.Buffer)
			simpleProgressElement := NewSimpleProgress(actualOut, &test.FakeStatsReporter)
			assert.Equal(t, test.expectedInterval, simpleProgressElement.intervalSeconds)
		})
	}
}

func TestProgress(t *testing.T) {
	tests := []struct {
		name               string
		mockSimpleProgress SimpleProgress
		staticTime         time.Time
		expectedOutput     string
	}{
		{
			name: "NewProgress: Good Call",
			mockSimpleProgress: SimpleProgress{
				periodStart:     time.Now().Add(-time.Hour * 1),
				intervalSeconds: 0.0,
				stats: &StatsReporter{
					Processed: 13,
					Errors:    0,
				},
			},
			staticTime:     time.Now(),
			expectedOutput: "Processed: \x1b[1m13\x1b[22m, Errors: 0",
		},
		{
			name: "NewProgress: No Risk",
			mockSimpleProgress: SimpleProgress{
				periodStart:     time.Now().Add(-time.Hour * 1),
				intervalSeconds: 0.0,
				stats: &StatsReporter{
					Processed: 5,
					Errors:    2,
				},
			},
			staticTime:     time.Now(),
			expectedOutput: "Processed: \x1b[1m5\x1b[22m, Errors: 2",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actualOut := new(bytes.Buffer)
			test.mockSimpleProgress.out = actualOut
			test.mockSimpleProgress.Progress()
			assert.NotEqual(t, test.staticTime, test.mockSimpleProgress.periodStart)
			assert.True(t, strings.HasSuffix(actualOut.String(), test.expectedOutput))
		})
	}
}
