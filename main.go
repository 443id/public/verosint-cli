// Copyright (c) "2025 Verosint, Inc"
package main

import "gitlab.com/verosint/public/verosint/cmd"

func main() {
	cmd.Execute()
}
