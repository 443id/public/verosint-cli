// Copyright (c) "2025 Verosint, Inc"
package utility

import (
	"os"
	"path/filepath"
	"strings"
)

func ServeFileAsString(fileName string) string {
	b, _ := os.ReadFile(filepath.Clean(fileName))
	str := string(b)
	str = strings.ReplaceAll(str, "\\n", "\n")
	return str
}
