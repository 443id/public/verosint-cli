// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/verosint/public/verosint/formatters"
)

type evaluator struct {
	recordProcessor func(ctx context.Context, record *formatters.Record)
}

func (p *evaluator) runE(cmd *cobra.Command, args []string) error {
	categories, err := parseCategories(args)
	if err != nil {
		return err
	}
	if len(categories) == 0 {
		return fmt.Errorf("values for at least one category of ip, phone or email must be provided, for example: ip:4.4.4.4")
	}

	formatter, err := formatters.NewJsonWriter(os.Stdout)
	if err != nil {
		return err
	}
	ctx := context.TODO()
	record := &formatters.Record{
		Identifiers: categories,
	}
	p.recordProcessor(ctx, record)
	_ = formatter.Write(record)
	return nil
}

func parseCategories(args []string) (map[string]string, error) {
	categories := make(map[string]string)
	for _, arg := range args {
		as := strings.Split(arg, ":")
		categoryName := as[0]
		if len(as) < 2 || !formatters.ValidIdentifiers[categoryName] {
			return nil, fmt.Errorf("invalid identifier: %s", arg)
		}

		if categories[categoryName] != "" {
			return nil, fmt.Errorf("multiple values for category %s provided", categoryName)
		}

		categoryValue := strings.TrimPrefix(arg, categoryName+":")
		categories[categoryName] = categoryValue
	}

	return categories, nil
}
