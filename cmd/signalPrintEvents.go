// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"context"

	"gitlab.com/verosint/public/verosint/client"
	"gitlab.com/verosint/public/verosint/formatters"

	"github.com/spf13/cobra"
)

func newBatchEvaluateEventsCommand(processor func(ctx context.Context, record *formatters.Record)) *cobra.Command {
	batchRunner := newBatchEvaluator(processor, []string{}, false)
	cmd := &cobra.Command{
		Use:   "send-events [input mappings]",
		Short: "Send events",
		Long: `Send events from various input formats.

Note: IP address, user agent, and event type are required for all
events.

By default, the column headers are mapped to the corresponding
identifiers (e.g. accountId column is mapped to accountId).

Default input mappings may be overridden by specifying mapping
arguments in <category name>:<attribute name> pairs, for example:

	verosint signalprint send-events email:mail

For CSV input type, column index may be used as well.  For example to
indicate that the email address is in the first column, use:

	verosint signalprint send-events email:0

Valid timestamp formats include:
	UNIX (number of seconds elapsed since January 1, 1970)
	RFC3339 (Date, Time, Hours offset from UTC)
	
Valid event types are: ACCOUNT_RECOVERY_FAILED,
ACCOUNT_RECOVERY_SUCCESS, CHANGE_EMAIL_FAILED, CHANGE_EMAIL_SUCCESS,
CHANGE_PASSWORD_FAILED, CHANGE_PASSWORD_SUCCESS,
CHANGE_PAYMENT_FAILED, CHANGE_PAYMENT_SUCCESS, CHANGE_PHONE_FAILED,
CHANGE_PHONE_SUCCESS, CHANGE_USERNAME_FAILED, CHANGE_USERNAME_SUCCESS,
EMAIL_SUCCESS, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT_FAILED,
LOGOUT_SUCCESS, MFA_DEVICE_UNENROLLED, MFA_ENROLLMENT_FAILED,
MFA_ENROLLMENT_SUCCESS, MFA_FAILED, MFA_STARTED, MFA_SUCCESS,
PAYMENT_ADDED_FAILED, PAYMENT_ADDED_SUCCESS, PAYMENT_FAILED,
PAYMENT_REMOVED_FAILED, PAYMENT_REMOVED_SUCCESS, PAYMENT_SUCCESS,
PUSH_NOTIFICATION_FAILED, PUSH_NOTIFICATION_SUCCESS,
SIGNUP_FAILED, SIGNUP_SUCCESS, SMS_FAILED, SMS_SUCCESS,
TOKEN_ISSUED_SUCCESS, VERIFICATION_SUCCESS`,

		RunE: batchRunner.runE,
	}

	batchRunner.addFlags(cmd)
	return cmd
}

func init() {
	processor := func(ctx context.Context, record *formatters.Record) {
		result := VerosintClient.IngestEvents(ctx, []client.Identifiers{record.Identifiers})
		record.RequestId = result.RequestId
		record.Error = result.Error
	}

	signalPrintCmd.AddCommand(newBatchEvaluateEventsCommand(processor))
}
