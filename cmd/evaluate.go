// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"fmt"

	"gitlab.com/verosint/public/verosint/client"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func evalPersistentPreRun(cmd *cobra.Command, args []string) error {
	apiKey := viper.GetString("apiKey")
	if apiKey == "" {
		return fmt.Errorf("verosint API Key must be provided with the --apiKey flag or in the config file")
	}
	VerosintClient = client.New(viper.GetString("apiKey"))
	if viper.GetString("baseUrl") != "" {
		client.BASE_URL = viper.GetString("baseUrl")
	}
	return nil
}

func NewEvaluateCommand(presistentPre func(cmd *cobra.Command, args []string) error) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "evaluate",
		Short: "Rules evaluation operations",
		Long: `Commands for evaluating rules.

NOTE: all rules commands require a valid Verosint API key.
Save the API key in the configuration file using the
"apikey: apikeyvalue" format.`,
		PersistentPreRunE: presistentPre,
	}
	requireApiKeyFlag(cmd)
	return cmd
}

func NewSignalPrintCommand(presistentPre func(cmd *cobra.Command, args []string) error) *cobra.Command {
	cmd := &cobra.Command{
		Use:               "signalprint",
		Short:             "SignalPrint Commands",
		Long:              `Commands for SignalPrint operations such as sending events`,
		PersistentPreRunE: presistentPre,
	}
	requireApiKeyFlag(cmd)
	return cmd
}

var (
	evaluateCmd    = NewEvaluateCommand(evalPersistentPreRun)
	signalPrintCmd = NewSignalPrintCommand(evalPersistentPreRun)
	VerosintClient *client.VerosintClient
)

func init() {
	rootCmd.AddCommand(evaluateCmd)
	rootCmd.AddCommand(signalPrintCmd)
}

func requireApiKeyFlag(cmd *cobra.Command) {
	_ = cmd.MarkFlagRequired("apiKey")
}
