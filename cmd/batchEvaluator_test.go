// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"bytes"
	"context"
	"errors"
	"os"
	"strings"
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"

	"gitlab.com/verosint/public/verosint/formatters"
	utility "gitlab.com/verosint/public/verosint/testingUtils"
)

var expectedCsvRecord = formatters.Record{
	ID: "0",
	Identifiers: map[string]string{
		"email": "user@example.com",
		"ip":    "8.8.8.8",
		"phone": "1-555-867-5309",
	},
	Parameters: map[string]interface{}{
		"title": "Manager",
	},
}

var expectedLdifRecord = formatters.Record{
	ID: "uid=user.0,ou=People,dc=example,dc=com",
	Identifiers: map[string]string{
		"email": "user@example.com",
		"phone": "1-555-867-5309",
	},
	Parameters: map[string]interface{}{
		"objectClass": []string{"top", "person", "inetOrgPerson", "uidObject"},
		"cn":          []string{"Joe Tester"},
		"sn":          []string{"Tester"},
		"uid":         []string{"user.0"},
	},
}

const (
	ldifTestFile string = "../testingUtils/testFiles/test.ldif"
	csvTestFile  string = "../testingUtils/testFiles/test.csv"
)

func TestBatchRisk(t *testing.T) {
	tests := []struct {
		name           string
		args           []string
		in             string
		inputType      string
		inputFile      string
		outputType     string
		outputFile     string
		reportFile     string
		expectedOut    string
		expectedErr    error
		expectedRecord formatters.Record
	}{
		{
			name:           "BatchEvaluatorRunE: csv stdin, csv stdout",
			args:           []string{},
			inputType:      "csv",
			outputType:     "csv",
			expectedRecord: expectedCsvRecord,
			in:             utility.ServeFileAsString(csvTestFile),
			expectedOut:    "id,",
		},
		{
			name:           "BatchEvaluatorRunE: csv file, csv stdout",
			args:           []string{},
			inputType:      "csv",
			inputFile:      csvTestFile,
			outputType:     "csv",
			expectedRecord: expectedCsvRecord,
			expectedOut:    "id,",
		},
		{
			name:           "BatchEvaluatorRunE: csv file, csv file",
			args:           []string{},
			inputType:      "csv",
			inputFile:      csvTestFile,
			outputType:     "csv",
			outputFile:     "output.csv",
			expectedRecord: expectedCsvRecord,
			expectedOut:    "\x1b[?25l\x1b[0G\x1b[2K∙∙∙ Processed: \x1b[1m1\x1b[22m",
		},
		{
			name:           "BatchEvaluatorRunE: ldif stdin, ldif stdout",
			args:           []string{},
			inputType:      "ldif",
			outputType:     "ldif",
			expectedRecord: expectedLdifRecord,
			in:             utility.ServeFileAsString(ldifTestFile),
			expectedOut:    "dn:",
		},
		{
			name:           "BatchEvaluatorRunE: ldif file, ldif file",
			args:           []string{},
			inputType:      "ldif",
			inputFile:      ldifTestFile,
			outputType:     "ldif",
			outputFile:     "output.ldif",
			expectedRecord: expectedLdifRecord,
			expectedOut:    "\x1b[?25l\x1b[0G\x1b[2K∙∙∙ Processed: \x1b[1m1\x1b[22m,",
		},
		{
			name:        "BatchEvaluatorRunE: Invalid input type",
			args:        []string{"mockArg:MockVal"},
			inputType:   "INVALID",
			expectedErr: errors.New("invalid input type: INVALID"),
		},
		{
			name:        "BatchEvaluatorRunE: Invalid output type",
			args:        []string{},
			inputType:   "csv",
			inputFile:   ldifTestFile,
			outputType:  "INVALID",
			expectedErr: errors.New("invalid output type: INVALID"),
		},
		{
			name:        "BatchEvaluatorRunE: Too Many Arguments!",
			args:        []string{"email:fakeEmail@company.co", "ip:4.4.4.4", "phone:7777777777", "email:anotherEmail@someco.org"},
			expectedErr: errors.New("multiple mappings for category email provided"),
		},
		{
			name:        "BatchEvaluatorRunE: Invalid identifier",
			args:        []string{"email"},
			expectedErr: errors.New("invalid mapping: email"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			batchEvaluator := &batchEvaluator{
				recordProcessor: func(ctx context.Context, record *formatters.Record) {
					assert.Equal(t, &test.expectedRecord, record)
				},
			}
			batchInputFile = test.inputFile
			batchInputType = test.inputType
			batchOutputFile = test.outputFile
			batchOutputType = test.outputType
			batchReportFile = test.reportFile

			cmd := &cobra.Command{
				Use: "test",
			}
			cmd.SetIn(strings.NewReader(test.in))
			actualOut := new(bytes.Buffer)
			cmd.SetOut(actualOut)

			err := batchEvaluator.runE(cmd, test.args)
			assert.True(t, strings.HasPrefix(actualOut.String(), test.expectedOut), actualOut.String())
			assert.Equal(t, test.expectedErr, err)
		})
	}
	os.Remove("verosint-report.json")
	os.Remove("output.csv")
	os.Remove("output.ldif")
}
