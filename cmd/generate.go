// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"github.com/spf13/cobra"
)

var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "File generation utilities",
	Long:  `Commands for generating examples that will help you get started using this tool`,
}

func init() {
	rootCmd.AddCommand(generateCmd)
}
