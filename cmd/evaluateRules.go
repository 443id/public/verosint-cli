// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/verosint/public/verosint/client"
	"gitlab.com/verosint/public/verosint/formatters"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	batchRunner     *batchEvaluator
	ruleSetFile     string
	rules           []client.Rule
	defaultOutcomes client.DefaultOutComes
	ruleSetUUID     string
	verbose         bool
)

type RuleFile []struct {
	UUID        string        `json:"uuid"`
	Name        string        `json:"name"`
	Description string        `json:"description"`
	Rules       []client.Rule `json:"rules"`
	Default     struct {
		Outcomes []string `json:"outcomes"`
	} `json:"default"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

func newBatchEvaluateRulesCommand(processor func(ctx context.Context, record *formatters.Record)) *cobra.Command {
	batchRunner = newBatchEvaluator(processor, []string{"outcomes", "reasons"}, true)
	cmd := &cobra.Command{
		Use:   "rules-batch [input mappings]",
		Short: "Batch rules evaluation",
		Long: `Batch evaluation of rules against Verosint signals from various input formats. 
	
Default input mappings for various formats:
	CSV - timestamp:timestamp ip:ip userAgent:userAgent accountId:accountId email:email phone:phone
	LDIF - email:mail phone:telephoneNumber 

Default input mappings may be overridden by specifying mapping arguments in
<category name>:<attribute name> pairs, for example: 

	verosint evaluate rules-batch email:mail

For CSV input type, column index may be used as well. For example to
indicate that the email address is in the first column, use:

	verosint evaluate rules-batch email:0`,
		PreRunE: checkRules,
		RunE:    batchRunExecutor,
	}

	batchRunner.addFlags(cmd)
	addFlags(cmd)

	return cmd
}

func batchRunExecutor(cmd *cobra.Command, args []string) error {
	if verbose {
		batchRunner.outputColumns = append(batchRunner.outputColumns, "signals")
	}
	return batchRunner.runE(cmd, args)
}

func newEvaluateRulesCommand(processor func(ctx context.Context, record *formatters.Record)) *cobra.Command {
	runner := evaluator{
		recordProcessor: processor,
	}
	cmd := &cobra.Command{
		Use:   "rules [identifiers]",
		Short: "Rules evaluation",
		Long: `Evaluate rules against Verosint signals for the provided account ID, email,
IP, phone number, and/or user agent string.

Identifier examples:

accountId:babsjensen
email:user@example.com
ip:1.1.1.1
phone:+18008291040
userAgent:"Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0"`,
		PreRunE: checkRules,
		RunE:    runner.runE,
		Example: `1. Get a rule evaluation on an IP address:
verosint evaluate rules ip:192.0.66.168 --ruleSetUuid your-unique-ruleset-uuid

2. Get a rule evaluation on an email address and a phone number:
verosint evaluate rules email:user@example.com phone:+18008291040 --ruleSetUuid your-unique-ruleset-uuid`,
	}

	addFlags(cmd)
	return cmd
}

func addFlags(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&ruleSetUUID, "ruleSetUuid", "s", "", "The UUID of the rule set to evaluate")
	cmd.Flags().StringVarP(&ruleSetFile, "ruleSetFile", "", "", "The rule set file to use")
	cmd.Flags().BoolVarP(&verbose, "verbose", "v", false, "Include signals in output if any rule evaluated to true")
}

func init() {
	processor := func(ctx context.Context, record *formatters.Record) {
		request := client.RulesEvaluationRequest{
			RuleSetUUID: ruleSetUUID,
			Rules:       rules,
			Default:     defaultOutcomes,
			Identifiers: record.Identifiers,
			Parameters:  record.Parameters,
			Verbose:     verbose,
		}

		result := VerosintClient.EvaluateRules(ctx, request)
		record.Outcomes = result.Outcomes
		record.Reasons = result.Reasons
		record.RequestId = result.RequestId
		record.Error = result.Error
		if verbose {
			record.Signals = result.Signals
		}
	}

	evaluateCmd.AddCommand(newEvaluateRulesCommand(processor))
	evaluateCmd.AddCommand(newBatchEvaluateRulesCommand(processor))
}

func checkRules(cmd *cobra.Command, args []string) error {
	if ruleSetFile != "" {
		return checkRulesFile(ruleSetFile)
	}

	if err := viper.UnmarshalKey("rules", &rules); err != nil {
		return fmt.Errorf("unable to unmarshal rules in config file: %w", err)
	}

	if ruleSetUUID == "" && rules == nil {
		return fmt.Errorf("UUID of rule set to evaluate must be specified using the --ruleSetUuid flag")
	}

	return nil
}

func checkRulesFile(rulesFileName string) error {
	rulesFile, err := loadRulesFile(rulesFileName)
	if err != nil {
		return err
	}

	rules = (*rulesFile)[0].Rules
	defaultOutcomes = client.DefaultOutComes{
		Outcomes: (*rulesFile)[0].Default.Outcomes,
	}

	// is it okay to have rules file but no UUID??
	if ruleSetUUID == "" && rules == nil {
		return fmt.Errorf("UUID of rule set to evaluate must be specified using the --ruleSetUuid flag")
	}

	if len(rules) == 0 {
		return fmt.Errorf("no rules found in rules file")
	}

	return nil
}

func loadRulesFile(fileName string) (*RuleFile, error) {
	file, err := os.ReadFile(filepath.Clean(fileName))
	if err != nil {
		return nil, fmt.Errorf("failed to read rules file: %w", err)
	}

	rulesFile := &RuleFile{}

	if err = json.Unmarshal(file, rulesFile); err != nil {
		return nil, fmt.Errorf("failed to unmarshal rules file into struct: %w", err)
	}

	return rulesFile, nil
}
