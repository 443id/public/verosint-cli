// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

type Generate struct {
	create         func(name string) (*os.File, error)
	formattedPrint func(w io.Writer, format string, a ...any) (n int, err error)
}

func (g *Generate) Create(name string) (*os.File, error) {
	return g.create(name)
}

func GenerateSchema(g *Generate) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "schema",
		Short: "Generate LDAP Schema",
		Long:  `Generate the LDAP schema to store 443ID risk scoring attributes in user entries`,
		RunE: func(cmd *cobra.Command, args []string) error {
			out := cmd.OutOrStdout()
			if generateSchemaOutputFile != "" {
				f, err := g.create(filepath.Clean(generateSchemaOutputFile))
				if err != nil {
					fmt.Fprintf(out, "Unable to create output file: %v\n", err)
					return err
				}
				out = f
			}

			outputTypesAndClasses := func(attrTypes []string, objClasses []string) {
				for _, attributeType := range attrTypes {
					_, _ = g.formattedPrint(out, "attributeTypes: %s\n", attributeType)
				}
				for _, objectClass := range objClasses {
					_, _ = g.formattedPrint(out, "objectClasses: %s\n", objectClass)
				}
			}

			var err error
			switch generateSchemaFormat {
			case "modify":
				_, err = g.formattedPrint(out, "dn: cn=schema\nchangetype: modify\nadd: attributeTypes\n")
				outputTypesAndClasses(getAttributeTypes(), getObjectClasses())
			case "entry":
				_, err = g.formattedPrint(out, "dn: cn=schema\nobjectClass: top\nobjectClass: ldapSubentry\nobjectClass: subschema\n")
				outputTypesAndClasses(getAttributeTypes(), getObjectClasses())
			default:
				msg := "Invalid output type: " + generateSchemaFormat
				fmt.Fprintf(out, "%s\n", msg)
				return errors.New(msg)
			}

			if err != nil {
				_, _ = os.Stdout.Write([]byte(err.Error() + "\n"))
				return err
			}
			return nil
		},
	}

	cmd.Flags().StringVarP(&generateSchemaFormat, "outputType", "o", "modify", "Whether to represent the schema as a \"modify\" operation or as an \"entry\"")
	cmd.Flags().StringVarP(&generateSchemaOutputFile, "outputFile", "p", "", "Output file. Defaults to stdout")

	return cmd
}

var generateSchemaCmd = GenerateSchema(&Generate{
	create:         os.Create,
	formattedPrint: fmt.Fprintf,
})

var (
	generateSchemaFormat     string
	generateSchemaOutputFile string
)

func init() {
	generateCmd.AddCommand(generateSchemaCmd)
}

func getAttributeTypes() []string {
	return []string{
		`( 1.3.6.1.4.1.59592.10 NAME 'verosint-timestamp'
    DESC 'Timestamp of last score received from https://api.verosint.com.'
    SINGLE-VALUE
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.2 NAME 'verosint-score'
    DESC 'Verosint RiskScore derived from https://api.verosint.com'
    SINGLE-VALUE
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.3 NAME 'verosint-levelTxt'
    DESC 'Text used when score doesn\27t need to be calculated.  HIGH/MEDIUM/LOW'
    SINGLE-VALUE
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.4 NAME 'verosint-levelNum'
    DESC 'Numerical representation of the levelTxt value. 1,2,3.'
    SINGLE-VALUE
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.9 NAME 'verosint-requestId'
    DESC 'Latest requestId used to when data is returned from https://api.verosint.com.'
    SINGLE-VALUE
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.10 NAME 'verosint-signals'
    DESC 'All signals returned about a users identifiers in JSON format.'
    SINGLE-VALUE
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.11 NAME 'verosint-outcomes'
    DESC 'Rule evaluation outcomes.'
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
		`( 1.3.6.1.4.1.59592.12 NAME 'verosint-reasons'
    DESC 'Rule evaluation reasons.'
    USAGE userApplications
    X-ORIGIN 'Verosint' )`,
	}
}

func getObjectClasses() []string {
	return []string{
		`( 1.3.6.1.4.1.59592.1 NAME 'verosint'
    DESC 'Verosint ObjectClass to hold Verosint Risk Data'
    AUXILIARY
    MAY ( verosint-levelNum $ verosint-levelTxt $ verosint-requestId $ verosint-score $ verosint-timestamp $ verosint-signals $ verosint-outcomes $ verosint-reasons ) 
    X-ORIGIN 'Verosint' )`,
	}
}
