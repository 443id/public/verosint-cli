// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"fmt"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	validRulesTestFile       = "../testingUtils/testFiles/valid-rules.json"
	missingUUIDRulesTestFile = "../testingUtils/testFiles/missingUUID-rules.json"
	noRulesTestFile          = "../testingUtils/testFiles/empty-rules.json"
	nonExistantFile          = "../testingUtils/testFiles/THISDOESNOTEXIST"
)

func TestCheckRulesFile(t *testing.T) {
	tests := []struct {
		name        string
		fileName    string
		expectedErr string
	}{
		{
			name:        "CheckRulesFile: Good Call",
			fileName:    validRulesTestFile,
			expectedErr: "",
		},
		{
			name:        "CheckRulesFile: Nonexistent file",
			fileName:    "fakeFile.json",
			expectedErr: "failed to read rules file: open fakeFile.json: no such file or directory",
		},
		{
			name:        "CheckRulesFile: File that does not exist",
			fileName:    nonExistantFile,
			expectedErr: fmt.Sprintf("failed to read rules file: open %s: no such file or directory", nonExistantFile),
		},
		{
			name: "CheckRulesFile: Missing UUID",
			// missing UUID is actually okay
			fileName:    missingUUIDRulesTestFile,
			expectedErr: "",
		},
		{
			name:        "CheckRulesFile: Invalid File",
			fileName:    noRulesTestFile,
			expectedErr: "no rules found in rules file",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := checkRulesFile(test.fileName)
			if err == nil && test.expectedErr == "" {
				return
			}
			if err == nil && test.expectedErr != "" {
				errMsg := fmt.Sprintf("expected: %s, got none", test.expectedErr)
				log.Fatal(errMsg)
			}
			assert.Equal(t, test.expectedErr, err.Error())
		})
	}
}
