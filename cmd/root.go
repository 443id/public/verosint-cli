// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/verosint/public/verosint/build"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "verosint",
	Short:   "Verosint CLI Utility",
	Version: build.Version(),
	Long: `Utility for interacting with rules evaluation.

Rules evaluation of a single set of identifiers or multiple identifiers
with batch operations are available. Input files using either CSV or
LDIF formats are supported.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		if cmd != updateCmd {
			periodicUpdateCheck()
		}
		if cmd == cmd.Root() {
			_ = cmd.Help()
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	defaultConfig := "~/.verosint.yaml"
	home, err := os.UserHomeDir()
	if err == nil {
		defaultConfig = fmt.Sprintf("%s%c.verosint.yaml", home, os.PathSeparator)
	}
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", fmt.Sprintf("configuration file (default is %s)", defaultConfig))
	rootCmd.PersistentFlags().String("apiKey", "", "Verosint API key")
	_ = viper.BindPFlag("apiKey", rootCmd.PersistentFlags().Lookup("apiKey"))
	rootCmd.CompletionOptions.DisableDefaultCmd = true
	rootCmd.SilenceUsage = true
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigType("yaml")
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".verosint" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".verosint")
	}

	// Allow the API key to be passed in as an environment variable in VEROSINT_APIKEY
	viper.SetEnvPrefix("VEROSINT")
	viper.AutomaticEnv()
	_ = viper.BindEnv("apiKey")

	_ = viper.ReadInConfig()
}
