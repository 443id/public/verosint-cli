// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/spf13/cobra"
	"golang.org/x/sync/errgroup"

	"gitlab.com/verosint/public/verosint/formatters"
	"gitlab.com/verosint/public/verosint/reporting"
	"gitlab.com/verosint/public/verosint/schema"
)

var (
	batchInputType  string
	batchInputFile  string
	batchOutputType string
	batchOutputFile string
	batchReportFile string
	continueOnError bool

	progressReporter reporting.ProgressReporter
)

type batchEvaluator struct {
	recordProcessor func(ctx context.Context, record *formatters.Record)
	signalSchemaUrl string
	outputColumns   []string
	generateReport  bool

	reader formatters.Reader
	writer formatters.Writer
	stats  *reporting.StatsReporter

	signalCh         chan os.Signal
	requestCtx       context.Context
	cancelRequestCtx context.CancelFunc
	pending          chan *formatters.Record
	finished         chan *formatters.Record
	readerGroup      sync.WaitGroup
	processGroup     sync.WaitGroup
	writerGroup      sync.WaitGroup
}

func newBatchEvaluator(processor func(ctx context.Context, record *formatters.Record), outColumns []string, generateReport bool) *batchEvaluator {
	return &batchEvaluator{
		recordProcessor: processor,
		outputColumns:   outColumns,
		generateReport:  generateReport,
	}
}

func (p *batchEvaluator) addFlags(cmd *cobra.Command) {
	cmd.Flags().StringVarP(&batchInputType, "inputType", "i", "csv", "Input format type: csv, ldif")
	cmd.Flags().StringVarP(&batchInputFile, "inputFile", "f", "", "Input file. Defaults to stdin")
	cmd.Flags().StringVarP(&batchOutputType, "outputType", "o", "csv", "Output format type: csv, ldif")
	cmd.Flags().StringVarP(&batchOutputFile, "outputFile", "p", "", "Output file. Defaults to stdout")
	if p.generateReport {
		cmd.Flags().StringVarP(&batchReportFile, "reportFile", "r", "verosint-report.json", "Report file")
	}
	cmd.Flags().BoolVarP(&continueOnError, "continueOnError", "c", false, "Continue batch process even if error is encountered")
}

func (p *batchEvaluator) runE(cmd *cobra.Command, args []string) error {
	inputMappings, err := p.parseMappings(args)
	if err != nil {
		return err
	}
	workdir, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("failed to read working directory: %w", err)
	}

	in, err := p.input(cmd, workdir, batchInputFile)
	if err != nil {
		return err
	}
	defer p.closeBatch(in)
	p.reader, err = p.newReader(in, inputMappings)
	if err != nil {
		return err
	}

	out, err := p.output(cmd, workdir, batchOutputFile)
	if err != nil {
		return err
	}
	defer p.closeBatch(out)
	p.writer, err = p.newWriter(out)
	if err != nil {
		return err
	}

	p.stats = reporting.NewStatsHandler()

	if p.generateReport {
		f, err := p.output(cmd, workdir, batchReportFile)
		if err != nil {
			return err
		}
		defer p.closeBatch(f)
		defer p.stats.JsonReport(f)
	}

	if batchOutputFile != "" {
		progressReporter = reporting.NewSimpleProgress(cmd.OutOrStdout(), p.stats)
		defer progressReporter.Close()
	}
	return p.process()
}

func (p *batchEvaluator) process() error {
	processorCount := 4

	p.signalCh = make(chan os.Signal, 1)
	p.pending = make(chan *formatters.Record)
	p.finished = make(chan *formatters.Record)

	signal.Notify(p.signalCh, os.Interrupt, syscall.SIGTERM)
	g, ctx := errgroup.WithContext(context.Background())
	p.requestCtx, p.cancelRequestCtx = context.WithCancel(ctx)

	// Reader routine
	p.readerGroup.Add(1)
	g.Go(p.readerRoutine)

	// GetScore routines
	for i := 0; i < processorCount; i++ {
		p.processGroup.Add(1)
		g.Go(p.processRoutine)
	}

	// Writer routine
	p.writerGroup.Add(1)
	g.Go(p.writerRoutine)

	p.readerGroup.Wait()
	close(p.pending)
	p.processGroup.Wait()
	close(p.finished)
	p.writerGroup.Wait()

	return g.Wait()
}

func (p *batchEvaluator) readerRoutine() error {
	defer p.readerGroup.Done()
	for {
		select {
		case <-p.signalCh:
			_, _ = os.Stdout.Write([]byte("Stopping...\n"))
			p.cancelRequestCtx()
			return fmt.Errorf("interrupted by user")
		default:
			record, err := p.reader.Read()

			if errors.Is(err, io.EOF) {
				return nil
			}

			if err != nil {
				_, _ = os.Stdout.Write([]byte(err.Error() + "\n"))
				return err
			}

			p.pending <- record
		}
	}
}

func (p *batchEvaluator) processRoutine() error {
	defer p.processGroup.Done()
	startTime := time.Time{}
	for record := range p.pending {
		duration := time.Since(startTime)
		if duration < 1*time.Second {
			time.Sleep(1*time.Second - duration)
		}
		startTime = time.Now()
		p.recordProcessor(p.requestCtx, record)
		p.finished <- record

		if record.Error != "" && !continueOnError {
			_, _ = os.Stderr.Write([]byte("Encountered error, stopping...\n"))
			p.signalCh <- os.Interrupt
			return errors.New(record.Error)
		}
	}
	return nil
}

func (p *batchEvaluator) writerRoutine() error {
	defer p.writerGroup.Done()
	for record := range p.finished {
		p.stats.Handle(record)
		if progressReporter != nil {
			progressReporter.Progress()
		}
		_ = p.writer.Write(record)
	}
	return nil
}

func (p *batchEvaluator) parseMappings(args []string) (map[string]string, error) {
	inputMappings := make(map[string]string)
	for _, mapping := range args {
		m := strings.Split(mapping, ":")
		if len(m) != 2 {
			return nil, fmt.Errorf("invalid mapping: %s", mapping)
		}
		if inputMappings[m[0]] != "" {
			return nil, fmt.Errorf("multiple mappings for category %s provided", m[0])
		}
		inputMappings[m[0]] = m[1]
	}
	return inputMappings, nil
}

func (p *batchEvaluator) input(cmd *cobra.Command, workdir, file string) (io.Reader, error) {
	if file != "" {
		file = filepath.Join(workdir, filepath.Clean(file))
		f, err := os.Open(file)
		if err != nil {
			return nil, fmt.Errorf("unable to open input file: %w", err)
		}
		return f, nil
	} else {
		return cmd.InOrStdin(), nil
	}
}

func (p *batchEvaluator) output(cmd *cobra.Command, workdir, file string) (io.Writer, error) {
	if file != "" {
		file = filepath.Join(workdir, filepath.Clean(file))
		f, err := os.Create(file)
		if err != nil {
			return nil, fmt.Errorf("unable to create output file: %w", err)
		}
		return f, nil
	} else {
		return cmd.OutOrStdout(), nil
	}
}

func (p *batchEvaluator) closeBatch(reader any) {
	if closer, ok := reader.(io.Closer); ok {
		if err := closer.Close(); err != nil && !errors.Is(err, os.ErrClosed) {
			_, _ = os.Stdout.Write([]byte(fmt.Sprintf("failed to close file '%s': %v\n", batchInputFile, err)))
		}
	}
}

func (p *batchEvaluator) newReader(in io.Reader, inputMappings map[string]string) (formatters.Reader, error) {
	switch batchInputType {
	case "csv":
		reader, err := formatters.NewCsvReader(in, inputMappings)
		if err != nil {
			return nil, err
		}
		return reader, nil
	case "ldif":
		reader, err := formatters.NewLdifReader(in, inputMappings)
		if err != nil {
			return nil, err
		}
		return reader, nil
	default:
		return nil, fmt.Errorf("invalid input type: %s", batchInputType)
	}
}

func (p *batchEvaluator) newWriter(out io.Writer) (formatters.Writer, error) {
	switch batchOutputType {
	case "csv":
		columns, err := defaultColumns(p.reader, p.outputColumns, p.signalSchemaUrl)
		if err != nil {
			return nil, err
		}
		writer, err := formatters.NewCsvWriter(out, columns)
		if err != nil {
			return nil, err
		}
		return writer, nil
	case "ldif":
		writer, err := formatters.NewLdifWriter(out)
		if err != nil {
			return nil, err
		}
		return writer, nil
	default:
		return nil, fmt.Errorf("invalid output type: %s", batchOutputType)
	}
}

func defaultColumns(reader formatters.Reader, outputPaths []string, signalSchemaUrl string) ([]string, error) {
	var signalSchema *schema.JsonSchema
	if signalSchemaUrl != "" {
		var err error
		signalSchema, err = schema.RetrieveSchema(signalSchemaUrl)
		if err != nil {
			return nil, err
		}
	}

	allColumns := make([]string, 0, 100)
	allColumns = append(allColumns, "id")
	allColumns = append(allColumns, reader.Paths()...)
	allColumns = append(allColumns, outputPaths...)
	if signalSchema != nil {
		allColumns = appendSignals(allColumns, "signals", signalSchema)
	}
	allColumns = append(allColumns, "version", "requestId", "error")

	return allColumns, nil
}

func hasIdentifier(signalPath string, slice []string) bool {
	for _, elem := range slice {
		if strings.HasPrefix(strings.Replace(signalPath, "signals", "identifiers", 1), elem) {
			return true
		}
	}
	return false
}

func appendSignals(slice []string, path string, schema *schema.JsonSchema) []string {
	// Only include a signal if there is an identifier for the signal category
	if hasIdentifier(path, slice) {
		switch schema.Type {
		case "object":
			for property, subSchema := range schema.Properties {
				slice = appendSignals(slice, path+"."+property, subSchema)
			}
			return slice
		default:
			return append(slice, path)
		}
	} else {
		return slice
	}
}

func toPointer[T any](v T) *T {
	return &v
}
