// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"

	"gitlab.com/verosint/public/verosint/build"
	"gitlab.com/verosint/public/verosint/client"
)

const (
	mockConfigFile          = "../testingUtils/testFiles/fakeConfig.yaml"
	mockConfigFileForWriter = "../testingUtils/testFiles/configWriterFile.yaml"
)

func TestConfigWriter(t *testing.T) {
	tempFile := "tmpfile"
	file, err := os.CreateTemp("", tempFile)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	defer os.Remove(tempFile)
	f, _ := os.OpenFile(mockConfigFileForWriter, os.O_APPEND|os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o600)
	defer f.Close()
	_, _ = io.Copy(file, f)
	tests := []struct {
		name        string
		expectedErr error
		inputFile   *os.File
	}{
		{
			name:        "ConfigWriter: Good Call",
			expectedErr: nil,
			inputFile:   f,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			viper.SetConfigFile(tempFile)
			viper.SetConfigType("yaml")
			err := updateLastChecked()
			assert.Equal(t, test.expectedErr, err)
		})
	}
}

func TestGetLatestVersion(t *testing.T) {
	tests := []struct {
		name             string
		httpResponseCode int
		httpHeader       string
		expectedErr      error
		expectedString   string
	}{
		{
			name:             "GetLatestVersion: Good Call",
			httpResponseCode: 302,
			httpHeader:       "https://gitlab.com/verosint/public/verosint/-/releases/v0.2.147",
			expectedErr:      nil,
			expectedString:   "v0.2.147",
		},
		{
			name:             "GetLatestVersion: Already Have Latest Version",
			httpResponseCode: 302,
			httpHeader:       "https://gitlab.com/verosint/public/verosint/-/releases/" + build.DefaultVersion,
			expectedErr:      nil,
			expectedString:   upToDate,
		},
		{
			name:             "GetLatestVersion: New Version Is older than current",
			httpResponseCode: 302,
			httpHeader:       "https://gitlab.com/verosint/public/verosint/-/releases/v0.0.0",
			expectedErr:      errors.New("misaligned versioning, latest release is younger than current"),
			expectedString:   "",
		},
		{
			name:             "GetLatestVersion: http request failed",
			httpHeader:       "",
			httpResponseCode: 400,
			expectedErr:      fmt.Errorf("failed to get latest version, got: %w", fmt.Errorf("bad response")),
			expectedString:   "",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			httpmock.RegisterResponder("GET", client.RELEASES_URL, func(r *http.Request) (*http.Response, error) {
				h := http.Header{}
				h.Add("location", test.httpHeader)
				return &http.Response{
					StatusCode: test.httpResponseCode,
					Header:     h,
				}, nil
			})

			httpmock.ActivateNonDefault(client.UpdateClient.GetClient())
			out, err := GetLatestVersion()
			assert.Equal(t, test.expectedString, out)
			assert.Equal(t, test.expectedErr, err)
			httpmock.Reset()
			httpmock.DeactivateAndReset()
		})
	}
}
