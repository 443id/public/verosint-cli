// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"time"

	"github.com/creativeprojects/go-selfupdate"
	"github.com/imroc/req/v3"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	upToDate     = "up to date"
	BINARIES_URL = "https://gitlab.com/verosint/public/verosint/-/releases/permalink/latest/downloads/verosint_"
)

func NewUpdateCommand(runner func(cmd *cobra.Command, args []string)) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "update",
		Short: "Application update",
		Long:  `Installs the latest verosint binary`,
		Run: func(cmd *cobra.Command, args []string) {
			if viper.GetBool("check") {
				checkUpdate()
			} else {
				runner(cmd, args)
			}
		},
	}
	addCheckUpdateFlag(cmd)
	return cmd
}

func addCheckUpdateFlag(cmd *cobra.Command) {
	cmd.PersistentFlags().Bool("check", false, "checks for update")
	_ = viper.BindPFlag("check", cmd.PersistentFlags().Lookup("check"))
}

var updateCmd = NewUpdateCommand(handleUpdate)

func init() {
	rootCmd.AddCommand(updateCmd)
}

func handleUpdate(cmd *cobra.Command, args []string) {
	latest, err := GetLatestVersion()
	if err != nil {
		_, _ = os.Stdout.Write([]byte(err.Error()))
		return
	}

	if latest == upToDate {
		msg := "You are already using the latest version of verosint\n"
		_, _ = os.Stdout.Write([]byte(msg))
		return
	}

	if err := update(); err != nil {
		errMsg := fmt.Sprintf("\nfailed to update got: %v\n", err)
		_, _ = os.Stdout.Write([]byte(errMsg))
		return
	}

	msg := fmt.Sprintf("Successfully updated verosint to version %s\n", latest)
	_, _ = os.Stdout.Write([]byte(msg))
}

func update() error {
	url := BINARIES_URL
	archMapping := map[string]string{
		"x64":   "x86_64",
		"386":   "i386",
		"arm64": "arm64",
		"amd64": "x86_64",
	}
	isDarwin := false
	switch runtime.GOOS {
	case "windows":
		url += "Windows_" + archMapping[runtime.GOARCH] + ".zip"
	case "linux":
		url += "Linux_" + archMapping[runtime.GOARCH] + ".tar.gz"
	case "darwin":
		isDarwin = true
		url += "MacOS_all.tar.gz"
	default:
		return errors.New("cannot update, unsupported os")
	}

	return doUpdate(url, isDarwin)
}

func doUpdate(url string, isDarwin bool) error {
	_, _ = os.Stdout.Write([]byte("Downloading latest release "))
	exe, err := os.Executable()
	if err != nil {
		return errors.New("could not locate executable path")
	}

	downloadComplete := make(chan bool)
	errChan := make(chan error)

	var cmd func(ctx context.Context, assetURL string, assetFileName string, cmdPath string) error
	if isDarwin {
		cmd = darwinUpdate
	} else {
		cmd = selfupdate.UpdateTo
	}
	go func() {
		if err = cmd(context.TODO(), url, "verosint", exe); err != nil {
			errChan <- err
			return
		}
		downloadComplete <- true
	}()

	done := false
	for !done {
		select {
		case done = <-downloadComplete:
			_, _ = os.Stdout.Write([]byte(" Done\n"))
			close(errChan)
		case err = <-errChan:
			return err
		default:
			_, _ = os.Stdout.Write([]byte("."))
			time.Sleep(time.Second)
		}
	}

	return nil
}

// go-selfupdate appears to have broke their support of universal macos binaries
// at some point, they have plans to support it in the future (https://github.com/creativeprojects/go-selfupdate/issues/37)
// but for now we will roll our own support
func darwinUpdate(ctx context.Context, url, assetFileName, cmdPath string) error {
	client := req.NewClient().SetLogger(nil)
	resp, err := client.R().
		SetContext(ctx).
		SetRetryCount(1).
		SetRetryFixedInterval(1*time.Second).
		SetHeader("Accept", "*/*").
		Get(url)
	if err != nil {
		return fmt.Errorf("failed to download release file from %s: %w", url, err)
	}

	dir := filepath.Dir(cmdPath)
	if err := extractSpecificFile(resp.Body, assetFileName, dir); err != nil {
		return err
	}

	return nil
}

func extractSpecificFile(gzipStream io.Reader, targetFile, destinationPath string) error {
	uncompressedStream, err := gzip.NewReader(gzipStream)
	if err != nil {
		return fmt.Errorf("failed to create gzip reader: %w", err)
	}
	defer uncompressedStream.Close()

	tarReader := tar.NewReader(uncompressedStream)
	for {
		header, err := tarReader.Next()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			return fmt.Errorf("failed to read entry: %w", err)
		}

		if header.Name == targetFile {
			destFilePath := filepath.Join(destinationPath, targetFile)
			if header.Typeflag == tar.TypeDir {
				return fmt.Errorf("expected file, got: directory")
			}
			if header.Typeflag == tar.TypeReg {
				f, err := os.OpenFile(destFilePath, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
				if err != nil {
					return fmt.Errorf("failed to create file: %w", err)
				}
				defer f.Close()

				for {
					_, err := io.CopyN(f, tarReader, 1024)
					if err != nil {
						if errors.Is(err, io.EOF) {
							break
						}
						return fmt.Errorf("failed to copy file: %w", err)
					}
				}
			} else {
				return fmt.Errorf("unknown type: %d in %s", header.Typeflag, header.Name)
			}

			return nil
		}
	}

	return fmt.Errorf("target file %s not found in archive", targetFile)
}
