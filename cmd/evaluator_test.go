// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"context"
	"errors"
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"

	"gitlab.com/verosint/public/verosint/formatters"
)

func TestRunE(t *testing.T) {
	tests := []struct {
		name           string
		args           []string
		commandName    string
		expectedErr    error
		expectedRecord formatters.Record
	}{
		{
			name:        "EvaluateRunE: Good Call",
			args:        []string{"email:fakeEmail@company.co", "ip:4.4.4.4", "phone:7777777777"},
			expectedErr: nil,
			expectedRecord: formatters.Record{
				Identifiers: map[string]string{
					"email": "fakeEmail@company.co",
					"ip":    "4.4.4.4",
					"phone": "7777777777",
				},
			},
		},
		{
			name:        "EvaluateRunE: Too Many Arguments!",
			args:        []string{"email:fakeEmail@company.co", "ip:4.4.4.4", "phone:7777777777", "email:anotherEmail@someco.org"},
			expectedErr: errors.New("multiple values for category email provided"),
		},
		{
			name:        "EvaluateRunE: Error Missing arguments",
			args:        []string{},
			expectedErr: errors.New("values for at least one category of ip, phone or email must be provided, for example: ip:4.4.4.4"),
		},
		{
			name:        "EvaluateRunE: Invalid identifier",
			args:        []string{"email"},
			expectedErr: errors.New("invalid identifier: email"),
		},
		{
			name:        "EvaluateRunE: Invalid identifier name",
			args:        []string{"emailaddress:babs@jensen.com"},
			expectedErr: errors.New("invalid identifier: emailaddress:babs@jensen.com"),
		},
		{
			name: "EvaluateRunE: all identifiers",
			args: []string{
				"accountId:babsjensen",
				"deviceId:1e91b8cf-ab36-4b38-bea4-3c799092d93f",
				"email:babs.jensen@example.com",
				"ip:2001:db8:3333:4444:5555:6666:7777:8888",
				"paymentHash:pp5jptserfk3zk4qy42tlucycrfwxhydvlemu9pqr93tuzlv9cc7g3s",
				"phone:+33476150200",
				"sessionId:CWv6sESLLnjD5QuiyTINEzvBH2XEvaC3",
				"targetApp:Dashboard",
				"userAgent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36",
			},
			expectedErr: nil,
			expectedRecord: formatters.Record{
				Identifiers: map[string]string{
					"accountId":   "babsjensen",
					"deviceId":    "1e91b8cf-ab36-4b38-bea4-3c799092d93f",
					"email":       "babs.jensen@example.com",
					"ip":          "2001:db8:3333:4444:5555:6666:7777:8888",
					"paymentHash": "pp5jptserfk3zk4qy42tlucycrfwxhydvlemu9pqr93tuzlv9cc7g3s",
					"phone":       "+33476150200",
					"sessionId":   "CWv6sESLLnjD5QuiyTINEzvBH2XEvaC3",
					"targetApp":   "Dashboard",
					"userAgent":   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36",
				},
			},
		},
		{
			name:        "EvaluateRunE: rules allows accountId and userAgent",
			args:        []string{"accountId:mabatche", "email:mabatche@gmail.com", "userAgent:Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0"},
			commandName: "rules",
			expectedErr: nil,
			expectedRecord: formatters.Record{
				Identifiers: map[string]string{
					"accountId": "mabatche",
					"email":     "mabatche@gmail.com",
					"userAgent": "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0",
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			evaluator := &evaluator{
				recordProcessor: func(ctx context.Context, record *formatters.Record) {
					assert.Equal(t, &test.expectedRecord, record)
				},
			}
			commandName := "test"
			if test.commandName != "" {
				commandName = test.commandName
			}
			cmd := &cobra.Command{
				Use: commandName,
			}
			err := evaluator.runE(cmd, test.args)
			assert.Equal(t, test.expectedErr, err)
		})
	}
}
