// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	utility "gitlab.com/verosint/public/verosint/testingUtils"
)

func serveFileAsSlice(fileName string) []string {
	fullStr := utility.ServeFileAsString(fileName)
	str := strings.ReplaceAll(fullStr, "\\\\27", "\\27")
	slice := strings.Split(str, ", ")
	return slice
}

const (
	generateSchemaFormatEntry string = "../testingUtils/testFiles/generateSchemaFormatEntry.txt"
	getObjectClassesGoodCall  string = "../testingUtils/testFiles/getObjectClassesGoodCall.txt"
	getAttributeTypesGoodCall string = "../testingUtils/testFiles/getAttributeTypesGoodCall.txt"
)

func TestGenerateSchema(t *testing.T) {
	tests := []struct {
		name         string
		args         []string
		mockGenerate Generate
		expectedOut  string
		flag         string
		outputFile   string
		expectError  bool
	}{
		{
			name: "GenerateSchema: generateSchemaFormat == modify",
			args: []string{"Random Argument"},
			mockGenerate: Generate{
				create: func(name string) (*os.File, error) {
					f, _ := os.Create(generateSchemaOutputFile)
					return f, nil
				},
				formattedPrint: func(w io.Writer, format string, a ...any) (n int, err error) {
					fmt.Fprint(w, format)
					return
				},
			},
			flag:        "modify",
			outputFile:  "delete_me",
			expectedOut: "",
			expectError: false,
		},
		{
			name: "GenerateSchema: generateSchemaFormat == entry",
			args: []string{"Random Argument"},
			mockGenerate: Generate{
				create: func(name string) (*os.File, error) {
					f, _ := os.Create(generateSchemaOutputFile)
					return f, nil
				},
				formattedPrint: func(w io.Writer, format string, a ...any) (n int, err error) {
					fmt.Fprint(w, format)
					return
				},
			},
			flag:        "entry",
			outputFile:  "",
			expectedOut: utility.ServeFileAsString(generateSchemaFormatEntry),
			expectError: false,
		},
		{
			name: "GenerateSchema: invalid output type",
			args: []string{"Random Argument"},
			mockGenerate: Generate{
				create: func(name string) (*os.File, error) {
					f, _ := os.Create(generateSchemaOutputFile)
					return f, nil
				},
				formattedPrint: func(w io.Writer, format string, a ...any) (n int, err error) {
					fmt.Fprint(w, format)
					return
				},
			},
			flag:        "",
			outputFile:  "",
			expectedOut: "Invalid output type: \n",
			expectError: true,
		},
		{
			name: `GenerateSchema: Unable to create output file`,
			args: []string{"Random Argument"},
			mockGenerate: Generate{
				create: func(name string) (*os.File, error) {
					_, _ = os.Create(generateSchemaOutputFile)
					return nil, errors.New("Fake Error")
				},
				formattedPrint: func(w io.Writer, format string, a ...any) (n int, err error) {
					fmt.Fprint(w, format)
					return
				},
			},
			flag:        "",
			outputFile:  "delete_me",
			expectedOut: "Unable to create output file: Fake Error\n",
			expectError: true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			generateSchemeCmd := GenerateSchema(&test.mockGenerate)
			generateSchemaFormat = test.flag
			generateSchemaOutputFile = test.outputFile
			b := bytes.NewBufferString("")
			generateSchemeCmd.SetOut(b)
			err := generateSchemeCmd.RunE(generateSchemeCmd, test.args)
			assert.Equal(t, test.expectError, err != nil)

			output, err := io.ReadAll(b)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectedOut, string(output))

			// clean up test artifact
			os.Remove("delete_me")
		})
	}
}

func TestGetObjectClasses(t *testing.T) {
	tests := []struct {
		name        string
		expectedOut []string
	}{
		{
			name:        "getObjectClasses/GoodCall",
			expectedOut: []string{utility.ServeFileAsString(getObjectClassesGoodCall)},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			out := getObjectClasses()
			assert.Equal(t, test.expectedOut, out)
		})
	}
}

func TestGetAttributeTypes(t *testing.T) {
	tests := []struct {
		name        string
		expectedOut []string
	}{
		{
			name:        "getAttributeTypes/GoodCall",
			expectedOut: serveFileAsSlice(getAttributeTypesGoodCall),
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			out := getAttributeTypes()
			assert.Equal(t, test.expectedOut, out)
		})
	}
}
