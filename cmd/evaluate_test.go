// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewEvaluateCommand(t *testing.T) {
	tests := []struct {
		name        string
		args        []string
		expectedErr error
	}{
		{
			name:        "EvaluateCmd: No ApiKey",
			args:        []string{},
			expectedErr: errors.New("verosint API Key must be provided with the --apiKey flag or in the config file"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			evaluate := NewEvaluateCommand(evalPersistentPreRun)
			err := evaluate.PersistentPreRunE(evaluate, test.args)
			assert.Equal(t, err, test.expectedErr)
		})
	}
}
