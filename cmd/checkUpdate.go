// Copyright (c) "2025 Verosint, Inc"
package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/hashicorp/go-version"
	"github.com/spf13/viper"

	"gitlab.com/verosint/public/verosint/build"
	"gitlab.com/verosint/public/verosint/client"
)

var (
	commonErrorMsg = []byte("failed to check for updates\n")
	lastCheck      = "lastUpdateCheck"
)

func periodicUpdateCheck() {
	lastUpdate := readLastChecked()

	if time.Since(lastUpdate).Hours() < 24 {
		return
	}

	if err := updateLastChecked(); err != nil {
		_, _ = os.Stdout.Write(commonErrorMsg)
		return
	}

	latest, err := GetLatestVersion()
	if err != nil {
		_, _ = os.Stdout.Write(commonErrorMsg)
		return
	}

	if latest == upToDate {
		return
	}

	updateMsg := fmt.Sprintf("New version is available run %s to upgrade to %s\n", `"verosint update"`, latest)
	_, _ = os.Stdout.Write([]byte(updateMsg))
}

func readLastChecked() time.Time {
	lastCheckedAt := viper.GetTime(lastCheck)

	if lastCheckedAt.IsZero() {
		// return 1 yr old date to ensure update happens
		return time.Now().AddDate(-1, 0, 0)
	}

	return lastCheckedAt
}

func updateLastChecked() error {
	viper.Set(lastCheck, time.Now().Format(time.DateTime))
	return viper.WriteConfig()
}

func askUser(message string) string {
	_, _ = os.Stdout.Write([]byte(message))
	response, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	response = strings.ToLower(response)
	response = strings.TrimSpace(response)
	return response
}

func promptForUpdate(version string) bool {
	msg := fmt.Sprintf("New update is available, would you like to upgrade to %s? (y/n)\n", version)
	response := askUser(msg)

	for response != "y" && response != "n" {
		response = askUser(msg + "Please enter a valid response\n")
	}

	return response == "y"
}

func GetLatestVersion() (string, error) {
	current := build.Version()

	latest, err := client.GetLatestCliVersion()
	if err != nil {
		return "", fmt.Errorf("failed to get latest version, got: %w", err)
	}

	if latest == current {
		return upToDate, nil
	}

	if latest == "" {
		return "", fmt.Errorf("failed to get latest version")
	}

	trimmedLatest := strings.ReplaceAll(latest, "v", "")
	latestVersion, err := version.NewVersion(trimmedLatest)
	if err != nil {
		return "", fmt.Errorf("malformed version: " + trimmedLatest)
	}

	trimmedCurrent := strings.ReplaceAll(current, "v", "")
	currentVersion, err := version.NewVersion(trimmedCurrent)
	if err != nil {
		return "", fmt.Errorf("malformed version: " + trimmedCurrent)
	}

	if latestVersion.LessThan(currentVersion) {
		return "", fmt.Errorf("misaligned versioning, latest release is younger than current")
	}

	return latest, nil
}

func checkUpdate() {
	latest, err := GetLatestVersion()
	if err != nil {
		_, _ = os.Stdout.Write([]byte(err.Error()))
		return
	}

	if latest == upToDate {
		msg := "You are already using the latest version of verosint\n"
		_, _ = os.Stdout.Write([]byte(msg))
		return
	}

	shouldUpdate := promptForUpdate(latest)
	if shouldUpdate {
		if err := update(); err != nil {
			_, _ = os.Stdout.Write([]byte(err.Error()))
			return
		}
	}
}
