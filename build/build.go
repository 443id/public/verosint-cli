// Copyright (c) "2025 Verosint, Inc"
package build

import (
	"runtime/debug"
)

var DefaultVersion = "v0.0.1"

func Version() string {
	info, ok := debug.ReadBuildInfo()
	if ok && info.Main.Version != "" && info.Main.Version != "(devel)" {
		return info.Main.Version
	}
	return DefaultVersion
}
