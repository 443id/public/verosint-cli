// Copyright (c) "2025 Verosint, Inc"
package build

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuild(t *testing.T) {
	tests := []struct {
		name           string
		expectedString string
	}{
		{
			name:           "Build: Normal Version Config",
			expectedString: DefaultVersion, // debug.ReadBuildInfo() reads a constant named version
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			v := Version()
			assert.Equal(t, test.expectedString, v)
		})
	}
}
