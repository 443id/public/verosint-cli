// Copyright (c) "2025 Verosint, Inc"
package schema

import (
	"embed"
	"encoding/json"
	"fmt"
	"net/url"
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/qri-io/jsonpointer"
)

//go:embed schemas/*
var schemaDir embed.FS

type JsonSchema struct {
	//nolint:tagliatelle // known reference
	Id         string                 `mapstructure:"$id,omitempty"`
	Title      string                 `mapstructure:"title,omitempty"`
	Type       string                 `mapstructure:"type,omitempty"`
	Items      *JsonSchema            `mapstructure:"items,omitempty"`
	Properties map[string]*JsonSchema `mapstructure:"properties,omitempty"`
	Format     string                 `mapstructure:"format,omitempty"`
}

func RetrieveSchema(urlString string) (*JsonSchema, error) {
	schemaUrl, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}
	// Retrieve the root schema
	var bytes []byte
	if schemaUrl.Scheme == "file" {
		bytes, err = schemaDir.ReadFile(strings.TrimPrefix(schemaUrl.Path, "/"))
		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("unknown scheme %s", schemaUrl.Scheme)
	}
	var rootSchema map[string]interface{}
	err = json.Unmarshal(bytes, &rootSchema)
	if err != nil {
		return nil, err
	}

	// Path into any sub-schemas in the root schema as specified on the URL
	var ptr jsonpointer.Pointer
	ptr, err = jsonpointer.Parse(schemaUrl.String())
	if err != nil {
		return nil, err
	}

	var subSchema interface{}
	subSchema, err = ptr.Eval(rootSchema)
	if err != nil {
		return nil, err
	}

	// TODO: Recursively resolve references
	schema := &JsonSchema{}
	err = mapstructure.Decode(subSchema, schema)
	if err != nil {
		return nil, err
	}
	return schema, nil
}
