// Copyright (c) "2025 Verosint, Inc"
package client

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/imroc/req/v3"

	"gitlab.com/verosint/public/verosint/build"
)

const (
	RELEASES_URL = "https://gitlab.com/verosint/public/verosint/-/releases/permalink/latest"
)

var UpdateClient = req.C().SetCommonHeader("User-Agent", USER_AGENT+build.Version()).
	SetCommonRetryCount(1).
	SetCommonRetryFixedInterval(1 * time.Second).
	SetTimeout(10 * time.Second).
	SetLogger(nil).
	SetRedirectPolicy(req.NoRedirectPolicy())

func GetLatestCliVersion() (string, error) {
	resp, err := UpdateClient.R().Get(RELEASES_URL)
	if err != nil && !errors.Is(err, http.ErrUseLastResponse) {
		return "", err
	} else if resp.GetStatusCode() != http.StatusFound {
		return "", fmt.Errorf("bad response")
	}

	return parseVersion(resp), nil
}

func parseVersion(res *req.Response) string {
	splitURL := strings.Split(res.Header.Get("location"), "releases/")
	if len(splitURL) == 2 {
		return splitURL[1]
	}
	return ""
}
