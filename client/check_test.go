// Copyright (c) "2025 Verosint, Inc"
package client

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
)

func TestGetVersion(t *testing.T) {
	tests := []struct {
		name             string
		httpHeader       string
		httpResponseCode int
		expectedVersion  string
		expectedError    error
	}{
		{
			name:             "GetVersion: Good Call",
			httpResponseCode: 302,
			httpHeader:       "https://gitlab.com/verosint/public/verosint/-/releases/v0.2.9",
			expectedVersion:  "v0.2.9",
			expectedError:    nil,
		},
		{
			name:             "GetVersion: Not Found",
			httpResponseCode: 404,
			httpHeader:       "",
			expectedVersion:  "",
			expectedError:    fmt.Errorf("bad response"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			httpmock.ActivateNonDefault(UpdateClient.GetClient())
			httpmock.RegisterResponder("GET", RELEASES_URL, func(r *http.Request) (*http.Response, error) {
				h := http.Header{}
				h.Add("location", test.httpHeader)
				return &http.Response{
					StatusCode: test.httpResponseCode,
					Header:     h,
				}, nil
			})
			gotVersion, err := GetLatestCliVersion()
			assert.Equal(t, test.expectedError, err)
			assert.Equal(t, test.expectedVersion, gotVersion)
		})
	}
}
