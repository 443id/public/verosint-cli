// Copyright (c) "2025 Verosint, Inc"
package client

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/verosint/public/verosint/build"
)

func TestNew(t *testing.T) {
	tests := []struct {
		name   string
		apiKey string
	}{
		{
			name:   "New: Good Call",
			apiKey: "FakeApiKey",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			newClient := New(test.apiKey)
			assert.Equal(t, test.apiKey, newClient.apiKey)
			assert.Equal(t, http.Header(http.Header{"Authorization": []string{"Bearer FakeApiKey"}, "Content-Type": []string{"application/json"}, "Accept": []string{"application/json"}, "User-Agent": []string{"verosint-cli/" + build.DefaultVersion}}), newClient.client.Headers)
		})
	}
}

func TestEvaluateRules(t *testing.T) {
	mockClient := New("fakeKey")
	tests := []struct {
		name                     string
		mockedResponse           []byte
		mockedStatus             int
		request                  RulesEvaluationRequest
		expectedEvaluationResult EvaluationResult
	}{
		{
			name: "GetSignals: Good Call",
			request: RulesEvaluationRequest{
				RuleSetUUID: "test",
				Rules: []Rule{
					{
						Name:     "Test Rule",
						Query:    "Test Query",
						Outcomes: []string{"OUTCOME"},
						Reason:   "Test Reason",
					},
				},
				Identifiers: Identifiers{
					"ip":    "4.4.4.4",
					"phone": "15555555555",
				},
				Parameters: map[string]interface{}{
					"foo": "bar",
				},
			},
			mockedResponse: []byte(`{
				"version":"1.0",
				"requestId":"superFakeRequestID",
				"outcomes": ["OUTCOME"],
				"reasons": ["reason"]
			}`),
			mockedStatus: 200,
			expectedEvaluationResult: EvaluationResult{
				RequestId: "superFakeRequestID",
				Outcomes:  []string{"OUTCOME"},
				Reasons:   []string{"reason"},
			},
		},
		{
			name: "GetScore: Invalid Call",
			request: RulesEvaluationRequest{
				Identifiers: Identifiers{
					"ip":    "4.4.4.4",
					"phone": "15555555555",
				},
			},
			mockedResponse: []byte(`{
				"error":"some error string"
			}`),
			mockedStatus: 400,
			expectedEvaluationResult: EvaluationResult{
				Error: "some error string",
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				assert.Equal(t, "/v1/rules/evaluate", r.URL.Path)
				assert.Equal(t, "application/json", r.Header.Get("Accept"))
				assert.Equal(t, "Bearer fakeKey", r.Header.Get("Authorization"))

				requestParam := &RulesEvaluationRequest{}
				json.NewDecoder(r.Body).Decode(requestParam)
				assert.Equal(t, test.request, *requestParam)

				w.Header().Add("content-type", "application/json")
				w.WriteHeader(test.mockedStatus)
				w.Write(test.mockedResponse)
			}))
			defer server.Close()
			BASE_URL = server.URL
			fakeScoreResult := mockClient.EvaluateRules(context.Background(), test.request)

			assert.Equal(t, &test.expectedEvaluationResult, fakeScoreResult)
		})
	}
}
