// Copyright (c) "2025 Verosint, Inc"
package client

import (
	"context"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/imroc/req/v3"

	"gitlab.com/verosint/public/verosint/build"
)

var (
	BASE_URL             = "https://api.verosint.com"
	RULE_EVALUTION_PATH  = "/v1/rules/evaluate"
	EVENT_INGESTION_PATH = "/v1/signalprint/events"
	USER_AGENT           = "verosint-cli/"
)

type VerosintClient struct {
	apiKey string
	client *req.Client
}

type Identifiers map[string]string

type Rule struct {
	Name     string   `json:"name,omitempty"`
	Query    string   `json:"query,omitempty"`
	Outcomes []string `json:"outcomes,omitempty"`
	Reason   string   `json:"reason,omitempty"`
}

type DefaultOutComes struct {
	Outcomes []string `json:"outcomes"`
}

type RulesEvaluationRequest struct {
	RuleSetUUID string                 `json:"ruleSetUuid"`
	Rules       []Rule                 `json:"rules"`
	Default     DefaultOutComes        `json:"default"`
	Identifiers Identifiers            `json:"identifiers"`
	Parameters  map[string]interface{} `json:"parameters"`
	Verbose     bool                   `json:"verbose"`
}

type EventIngestionRequest []Identifiers

type EventIngestionResponse struct {
	RequestId string `json:"requestId,omitempty"`
	Error     string `json:"error,omitempty"`
}

type EvaluationErrors struct {
	Error string `json:"error,omitempty"`
	Rule  Rule   `json:"rule,omitempty"`
}

type EvaluationResult struct {
	RequestId string                            `json:"requestId,omitempty"`
	Outcomes  []string                          `json:"outcomes,omitempty"`
	Reasons   []string                          `json:"reasons,omitempty"`
	Errors    []EvaluationErrors                `json:"errors,omitempty"`
	Error     string                            `json:"error,omitempty"`
	Signals   map[string]map[string]interface{} `json:"signals,omitempty"`
}

func New(apiKey string) *VerosintClient {
	client := req.C()
	client.SetCommonHeader("Content-Type", "application/json")
	client.SetCommonHeader("Accept", "application/json")
	client.SetCommonHeader("User-Agent", USER_AGENT+build.Version())
	client.SetCommonHeader("Authorization", "Bearer "+apiKey)
	client.SetRedirectPolicy(req.NoRedirectPolicy())

	client.SetCommonRetryCount(1)
	client.SetCommonRetryFixedInterval(1 * time.Second)
	client.SetTimeout(10 * time.Second)
	client.SetCommonRetryCondition(func(r *req.Response, err error) bool {
		if err != nil {
			return false
		}
		if r.GetStatusCode() == http.StatusOK && strings.Contains(r.String(), "UNKNOWN") {
			return true
		}
		if r.GetStatusCode() >= 500 {
			return true
		}
		return false
	})

	// Diable HTTP client logging
	client.SetLogger(nil)

	return &VerosintClient{
		apiKey: apiKey,
		client: client,
	}
}

func (r *VerosintClient) EvaluateRules(ctx context.Context, request RulesEvaluationRequest) *EvaluationResult {
	evaluationResult := &EvaluationResult{}
	_, _ = r.client.R().SetContext(ctx).
		SetBody(request).
		SetSuccessResult(evaluationResult).
		SetErrorResult(evaluationResult).
		Post(r.url(RULE_EVALUTION_PATH))

	return evaluationResult
}

func (r *VerosintClient) IngestEvents(ctx context.Context, request EventIngestionRequest) *EventIngestionResponse {
	eventIngestionResponse := &EventIngestionResponse{}
	_, _ = r.client.R().SetContext(ctx).
		SetBody(request).
		SetSuccessResult(eventIngestionResponse).
		SetErrorResult(eventIngestionResponse).
		Post(r.url(EVENT_INGESTION_PATH))

	return eventIngestionResponse
}

type Operation struct {
	Op    string   `json:"op"`
	Path  string   `json:"path"`
	Value []string `json:"value,omitempty"`
}
type Patch struct {
	Operations []Operation `json:"operations"`
}

func (r *VerosintClient) url(elems ...string) string {
	url, _ := url.JoinPath(BASE_URL, elems...)
	return url
}
