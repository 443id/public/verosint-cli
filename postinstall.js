#!/usr/bin/env node
// Copyright (c) "2025 Verosint, Inc"
"use strict"

const { join } = require('path');
const { spawnSync } = require('child_process');
const { existsSync, chmodSync, copyFileSync, unlinkSync, createWriteStream, readFileSync } = require('fs');
const mkdirp = require('mkdirp');
const axios = require('axios')
const tar = require('tar');
const zlib = require('zlib');
const unzipper = require('unzipper');

// Mapping from Node's `process.arch` to Our binary arch standard
const ARCH_MAPPING = {
    "x64": "x86_64",
    "x32": "i386",
    "arm64": "arm64",
    "amd64": "x86_64"
};

// Mapping between Node's `process.platform` to Our binary platform standard
const PLATFORM_MAPPING = {
    "darwin": "MacOS",
    "linux": "Linux",
    "win32": "Windows",
};

function getInstallationPath(callback) {
    // `npm bin` will output the path where binary files should be installed
    const {err, stdout, stderr} = spawnSync('npm bin');
    let dir = null;
    if (err || stderr || !stdout || stdout.length === 0) {
        // We couldn't infer path from `npm bin`. Let's try to get it from
        // Environment variables set by NPM when it runs.
        // npm_config_prefix points to NPM's installation directory where `bin` folder is available
        // Ex: /Users/foo/.nvm/versions/node/v4.3.0
        const env = process.env;
        if (env?.npm_config_prefix) {
            dir = join(env.npm_config_prefix, 'bin');
        } else {
            return callback(new Error('Error finding binary installation directory'));
        }
    } else {
        dir = stdout.trim();
    }

    dir = dir.replace(/node_modules.*[/\\]\.bin/, join('node_modules', '.bin'));
    mkdirp.sync(dir);
    callback(null, dir);
}

function verifyAndPlaceBinary(binName, callback) {
    if (!existsSync(binName)) {
      return callback(`Downloaded binary does not contain the binary specified in configuration - ${binName}`);
    }
  
    getInstallationPath((err, installationPath) => {
        if (err) {
          return callback(err);
        }
  
        // Move the binary file and make sure it is executable
        copyFileSync(binName, join(installationPath, binName));
        unlinkSync(binName);
        chmodSync(join(installationPath, binName), '755');
  
        console.log('Placed binary on', join(installationPath, binName));
  
        callback(null);
    });
}

function validateConfiguration(packageJson) {

    if (!packageJson.version) {
        return "'version' property must be specified";
    }

    if (!packageJson.goBinary || typeof(packageJson.goBinary) !== "object") {
        return "'goBinary' property must be defined and be an object";
    }

    if (!packageJson.goBinary.name) {
        return "'name' property is necessary";
    }

    if (!packageJson.goBinary.path) {
        return "'path' property is necessary";
    }

    if (!packageJson.goBinary.url) {
        return "'url' property is required";
    }
}

function parsePackageJson() {
    if (!(process.arch in ARCH_MAPPING)) {
        console.error("Installation is not supported for this architecture: " + process.arch);
        return;
    }

    if (!(process.platform in PLATFORM_MAPPING)) {
        console.error("Installation is not supported for this platform: " + process.platform);
        return
    }

    const packageJsonPath = join(".", "package.json");
    if (!existsSync(packageJsonPath)) {
        console.error("Unable to find package.json. " +
            "Please run this script at root of the package you want to be installed");
        return
    }

    let packageJson = JSON.parse(readFileSync(packageJsonPath));
    let error = validateConfiguration(packageJson);
    if (error && error.length > 0) {
        console.error("Invalid package.json: " + error);
        return
    }

    // We have validated the config. It exists in all its glory
    let binName = packageJson.goBinary.name;
    let binPath = packageJson.goBinary.path;
    let url = packageJson.goBinary.url;
    let version = packageJson.version;

    // Binary name on Windows has .exe suffix
    if (process.platform === "win32") {
        binName += ".exe";
        url = url.replace(/{{win_ext}}/g, '.exe');
    } else {
        url = url.replace(/{{win_ext}}/g, '');
    }

    url = url.replace(/{{version}}/g, version);

    switch(PLATFORM_MAPPING[process.platform]) {
        case "MacOS":
            url = url + "MacOS_all.tar.gz";
            break;
        case "Linux":
            url = url + "Linux_" + ARCH_MAPPING[process.arch] + ".tar.gz";
            break;
        case "Windows":
            url = url + "Windows_" + ARCH_MAPPING[process.arch] + ".zip";
            break;
        default:
            console.error(`You have an unsupported platform (${process.platform}) or architecture (${process.arch})`);
            return;
    }

    return {
        binName: binName,
        binPath: binPath,
        url: url,
        version: version
    }
}

function unzip({ opts, req, onSuccess, onError }) {
    const unzip = unzipper.Extract({ path: opts.binPath });
    unzip.on('error', onError);
    unzip.on('close', onSuccess);
    req.pipe(unzip);
}

function untar({ opts, req, onSuccess, onError }) {
    const ungz = zlib.createGunzip();
    const untar = tar.extract({ path: opts.binPath });
    ungz.on('error', onError);
    untar.on('error', onError);
    untar.on('end', onSuccess);
    req.pipe(ungz).pipe(untar);
}

/**
 * Move strategy for binary resources without compression.
 */
function move({ opts, req, onSuccess, onError }) {
    const stream = createWriteStream(join(opts.binPath, opts.binName));
    stream.on('error', onError);
    stream.on('close', onSuccess);
    req.pipe(stream);
}

function getStrategy({ url }) {
    if (url.endsWith('.tar.gz')) {
        return untar;
    } else if (url.endsWith('.zip')) {
        return unzip;
    } else {
        return move;
    }
}

/**
 * Reads the configuration from application's package.json,
 * validates properties, downloads the binary, untars, and stores at
 * ./bin in the package's root. NPM already has support to install binary files
 * specific locations when invoked with "npm install -g"
 *
 *  See: https://docs.npmjs.com/files/package.json#bin
 */

function install(callback) {
    const opts = parsePackageJson();
    if (!opts) return callback('Invalid inputs');
    mkdirp.sync(opts.binPath);
    console.log('Downloading from URL: ' + opts.url);

    axios.get( opts.url, {
        responseType: 'stream'
    } )
        .then(response => {
            if (response.status !== 200) return callback('Error downloading binary. HTTP Status Code: ' + response.status);
            const strategy = getStrategy(opts);
            strategy({
                opts,
                req: response.data,
                onSuccess: () => verifyAndPlaceBinary(opts.binName, callback),
                onError: callback
            });
        })
        .catch(function( err ) {
            callback(`Error downloading binary from ${opts.url} error: ${err}`);
        });
}
  

function uninstall(callback) {
    const { binName } = parsePackageJson();
    getInstallationPath((err, installationPath) => {
        if (err) {
            return callback(err);
        }
        try {
            unlinkSync(join(installationPath, binName));
        } catch(ex) {
            // Ignore errors when deleting the file.
        }
        return callback(null);
    });
}
  


// Parse command line arguments and call the right method
let actions = {
    "install": install,
    "uninstall": uninstall
};

let argv = process.argv;
if (argv && argv.length > 2) {
    let cmd = process.argv[2];
    if (!actions[cmd]) {
        console.log("Invalid command to postinstall script. `install` and `uninstall` are the only supported commands");
        process.exit(1);
    }

    actions[cmd](function(err) {
        if (err) {
            console.error(err);
            process.exit(1);
        } else {
            process.exit(0);
        }
    });
} else {
    console.log('No command supplied. `install` and `uninstall` are the only supported commands');
    exit(1);
}
