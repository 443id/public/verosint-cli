// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"testing"

	"github.com/go-ldap/ldif"
	"github.com/stretchr/testify/assert"

	utility "gitlab.com/verosint/public/verosint/testingUtils"
)

const ldifTestFile string = "../testingUtils/testFiles/test.ldif"

var expectedLdifRecord = Record{
	ID:          "uid=user.0,ou=People,dc=example,dc=com",
	Identifiers: map[string]string{},
	Parameters: map[string]interface{}{
		"cn":              []string{"Joe Tester"},
		"mail":            []string{"user@example.com"},
		"objectClass":     []string{"top", "person", "inetOrgPerson", "uidObject"},
		"sn":              []string{"Tester"},
		"telephonenumber": []string{"1-555-867-5309"},
		"uid":             []string{"user.0"},
	},
	Version:   "",
	Signals:   map[string]map[string]interface{}(nil),
	RequestId: "",
	Error:     "",
	Outcomes:  []string(nil),
	Reasons:   []string(nil),
}

var secondExpectedLdifRecord = &Record{
	ID: "uid=user.0,ou=People,dc=example,dc=com",
	Identifiers: map[string]string{
		"email": "user@example.com",
		"phone": "1-555-867-5309",
	},
	Parameters: map[string]interface{}{
		"cn":          []string{"Joe Tester"},
		"objectClass": []string{"top", "person", "inetOrgPerson", "uidObject"},
		"sn":          []string{"Tester"},
		"uid":         []string{"user.0"},
	},
	Version:   "",
	Signals:   map[string]map[string]interface{}(nil),
	RequestId: "",
	Error:     "",
	Outcomes:  []string(nil),
	Reasons:   []string(nil),
}

func TestNewLdifReader(t *testing.T) {
	tests := []struct {
		name           string
		mockReader     io.Reader
		mockMappings   map[string]string
		expectedReader *LdifReader
		expectedError  error
	}{
		{
			name:         "NewLdifReader: Filled Map",
			mockReader:   &bytes.Buffer{},
			mockMappings: map[string]string{"email": "mail", "phone": "telephoneNumber"},
			expectedReader: &LdifReader{
				ldif:     &ldif.LDIF{},
				line:     0,
				mappings: map[string]string{"email": "mail", "phone": "telephoneNumber"},
			},
			expectedError: nil,
		},
		{
			name:         "NewLdifReader: Empty Map",
			mockReader:   &bytes.Buffer{},
			mockMappings: map[string]string{},
			expectedReader: &LdifReader{
				ldif:     &ldif.LDIF{},
				line:     0,
				mappings: map[string]string{"email": "mail", "phone": "telephoneNumber"},
			},
			expectedError: nil,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			newReader, err := NewLdifReader(test.mockReader, test.mockMappings)
			assert.Equal(t, test.expectedError, err)
			assert.Equal(t, test.expectedReader.mappings, newReader.mappings)
		})
	}
}

func TestHandleDefault(t *testing.T) {
	tests := []struct {
		name              string
		firstChar         byte
		line              string
		isComment         bool
		expectedLine      string
		expectedIsComment bool
	}{
		{
			name:              "HandleDefault: Has Space",
			firstChar:         space,
			line:              " hello",
			isComment:         false,
			expectedLine:      "hello",
			expectedIsComment: false,
		},
		{
			name:              "HandleDefault: No Space",
			firstChar:         'h',
			line:              "hello",
			isComment:         false,
			expectedLine:      "hello",
			expectedIsComment: false,
		},
		{
			name:              "HandleDefault: No Space",
			firstChar:         'h',
			line:              "hello",
			isComment:         true,
			expectedLine:      "hello",
			expectedIsComment: false,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			outLine, outComment := handleDefault(test.firstChar, test.expectedLine, test.isComment)
			assert.Equal(t, test.expectedLine, outLine)
			assert.Equal(t, test.expectedIsComment, outComment)
		})
	}
}

func TestUnmarshal(t *testing.T) {
	tests := []struct {
		name           string
		ldifReader     *LdifReader
		reader         *bytes.Buffer
		eof            bool
		expectedRecord *Record
		expectedError  error
	}{
		{
			name: "Unmarshal: At End Of File",
			ldifReader: &LdifReader{
				scanner:  *bufio.NewScanner(&bytes.Buffer{}),
				ldif:     &ldif.LDIF{},
				line:     1,
				mappings: map[string]string{},
			},
			reader:         bytes.NewBufferString(utility.ServeFileAsString(ldifTestFile)),
			eof:            false,
			expectedRecord: &expectedLdifRecord,
			expectedError:  nil,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			record, err := test.ldifReader.Unmarshal(test.reader, test.eof)
			assert.Equal(t, test.expectedRecord, record)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestLdifRead(t *testing.T) {
	f, _ := os.Open(ldifTestFile)

	tests := []struct {
		name           string
		mockReader     *LdifReader
		expectedRecord *Record
		expectedError  error
	}{
		{
			name: "Read: Normal",
			mockReader: &LdifReader{
				scanner:  *bufio.NewScanner(f),
				ldif:     &ldif.LDIF{},
				line:     0,
				mappings: map[string]string{"email": "mail", "phone": "telephoneNumber"},
			},
			expectedRecord: secondExpectedLdifRecord,
			expectedError:  nil,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			record, err := test.mockReader.Read()
			assert.Equal(t, test.expectedError, err)
			assert.Equal(t, test.expectedRecord, record)
		})
	}
}
