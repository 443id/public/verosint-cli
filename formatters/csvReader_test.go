// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCsvReader(t *testing.T) {
	tests := []struct {
		name              string
		in                string
		mappings          map[string]string
		expectedCsvReader *CsvReader
		expectedErr       error
		expectedPaths     []string
	}{
		{
			name:              "NewCsvReader: Failed to Read csvReader",
			in:                "",
			mappings:          map[string]string{},
			expectedCsvReader: (*CsvReader)(nil),
			expectedErr:       errors.New("EOF"),
		},
		{
			name:              "NewCsvReader: Invalid mapping",
			in:                "header1,header2,",
			mappings:          map[string]string{"email": "header3"},
			expectedCsvReader: (*CsvReader)(nil),
			expectedErr:       errors.New("No header with name header3"),
		},
		{
			name: "NewCsvReader: Good default headers",
			// Make sure invisible characters are removed correctly
			in:       string([]byte{239, 187, 191, 105, 112}) + ",phone,email,extra",
			mappings: map[string]string{},
			expectedCsvReader: &CsvReader{
				reader:           csv.NewReader(strings.NewReader("ip,phone,email,extra")),
				indexes:          map[string]int{"email": 2, "ip": 0, "phone": 1},
				parameterIndexes: map[string]int{"extra": 3},
				rowIndex:         0,
			},
			expectedErr:   nil,
			expectedPaths: []string{"identifiers.email", "identifiers.ip", "identifiers.phone", "parameters.extra"},
		},
		{
			name:     "NewCsvReader: Good explicit headers",
			in:       "IP Address,Phone Number,Email,extra",
			mappings: map[string]string{"ip": "IP Address", "email": "Email"},
			expectedCsvReader: &CsvReader{
				reader:           csv.NewReader(strings.NewReader("IP Address,Phone Number,Email,extra")),
				indexes:          map[string]int{"email": 2, "ip": 0},
				parameterIndexes: map[string]int{"Phone Number": 1, "extra": 3},
				rowIndex:         0,
			},
			expectedErr:   nil,
			expectedPaths: []string{"identifiers.email", "identifiers.ip", "parameters.Phone Number", "parameters.extra"},
		},
		{
			name:     "NewCsvReader: Trimming whitespace",
			in:       " ip,  Phone Number,Email , extra",
			mappings: map[string]string{},
			expectedCsvReader: &CsvReader{
				reader:           csv.NewReader(strings.NewReader(" ip,  Phone Number,Email , extra")),
				indexes:          map[string]int{"ip": 0},
				parameterIndexes: map[string]int{"Phone Number": 1, "email": 2, "extra": 3},
				rowIndex:         0,
			},
			expectedErr:   nil,
			expectedPaths: []string{"identifiers.ip", "parameters.Email", "parameters.Phone Number", "parameters.extra"},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			reader, err := NewCsvReader(strings.NewReader(test.in), test.mappings)
			assert.Equal(t, err, test.expectedErr)
			if err == nil {
				assert.Equal(t, test.expectedCsvReader.indexes, reader.indexes)
				assert.Equal(t, test.expectedCsvReader.rowIndex, reader.rowIndex)
				paths := reader.Paths()
				sort.Strings(paths)
				assert.Equal(t, test.expectedPaths, paths)
			}
		})
	}
}

func TestRead(t *testing.T) {
	tests := []struct {
		name           string
		fakeCsvReader  CsvReader
		expectedRecord Record
		expectedError  error
		in             string
	}{
		{
			name: "Read: Error: Empty Buffer",
			fakeCsvReader: CsvReader{
				indexes:  map[string]int{"email": 0, "ip": 0, "phone": 0},
				rowIndex: 0,
			},
			expectedRecord: Record{},
			expectedError:  errors.New("EOF"),
		},
		{
			name: "Read: Good Call",
			fakeCsvReader: CsvReader{
				indexes:  map[string]int{"email": 0, "ip": 0, "phone": 0},
				rowIndex: 0,
			},
			expectedRecord: Record{
				ID: fmt.Sprintf("%v", 0),
				Identifiers: map[string]string{
					"email": "Test String",
					"ip":    "Test String",
					"phone": "Test String",
				},
				Parameters: map[string]interface{}{},
			},
			expectedError: nil,
			in:            "Test String,Test String,Test String",
		},
		{
			name: "Read: Good Call With Additional Whitespace",
			fakeCsvReader: CsvReader{
				indexes:  map[string]int{"email": 0, "ip": 0, "phone": 0},
				rowIndex: 0,
			},
			expectedRecord: Record{
				ID: fmt.Sprintf("%v", 0),
				Identifiers: map[string]string{
					"email": "Test String",
					"ip":    "Test String",
					"phone": "Test String",
				},
				Parameters: map[string]interface{}{},
			},
			expectedError: nil,
			in:            "Test String		,  Test String,    Test String  ",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var r io.Reader = strings.NewReader(test.in)
			test.fakeCsvReader.reader = csv.NewReader(r)

			record, err := test.fakeCsvReader.Read()

			if err != nil {
				assert.Equal(t, err, test.expectedError)
			} else {
				assert.Equal(t, &test.expectedRecord, record)
				assert.Equal(t, test.expectedError, err)
			}
		})
	}
}

func TestGetIndexForAttr(t *testing.T) {
	tests := []struct {
		name          string
		expectedError error
		expectedIndex int
		attr          string
		headers       []string
	}{
		{
			name:          "getIndexForAttr: No Attribute",
			attr:          "",
			headers:       []string{},
			expectedError: nil,
			expectedIndex: -1,
		},
		{
			name:          "getIndexForAttr: No Header",
			attr:          "L",
			headers:       []string{},
			expectedError: errors.New("No header with name L"),
			expectedIndex: -1,
		},
		{
			name:          "getIndexForAttr: strconv.Atoi Error",
			attr:          "1",
			headers:       []string{},
			expectedError: nil,
			expectedIndex: 1,
		},
		{
			name:          "getIndexForAttr: Good Call",
			attr:          "found me",
			headers:       []string{"Random", " ", "found me"},
			expectedError: nil,
			expectedIndex: 2,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			index, err := getIndexForAttr(test.attr, test.headers)

			assert.Equal(t, test.expectedError, err)
			assert.Equal(t, test.expectedIndex, index)
		})
	}
}
