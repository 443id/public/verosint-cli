// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"encoding/json"
	"io"
)

type JsonWriter struct {
	encoder *json.Encoder
}

//nolint:unparam // consistency with other writers
func NewJsonWriter(writer io.Writer) (*JsonWriter, error) {
	encoder := json.NewEncoder(writer)
	encoder.SetIndent("", "  ")
	return &JsonWriter{
		encoder: encoder,
	}, nil
}

func (w *JsonWriter) Write(record any) error {
	err := w.encoder.Encode(record)
	if err != nil {
		return err
	}
	return nil
}
