// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewLdifWriter(t *testing.T) {
	tests := []struct {
		name           string
		expectedWriter *LdifWriter
		expectedError  error
		writer         io.Writer
	}{
		{
			name: "NewLdifWriter: Good Call",
			expectedWriter: &LdifWriter{
				writer: new(bytes.Buffer),
			},
			expectedError: nil,
			writer:        new(bytes.Buffer),
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			newWriter, err := NewLdifWriter(test.writer)
			assert.Equal(t, test.expectedWriter, newWriter)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestLdifWrite(t *testing.T) {
	tests := []struct {
		name               string
		mockLdifWriter     *LdifWriter
		mockRecord         *Record
		expectedError      error
		originalLdifWriter *LdifWriter
	}{
		{
			name: "Write: Good Call",
			mockLdifWriter: &LdifWriter{
				writer: new(bytes.Buffer),
			},
			mockRecord: &Record{
				ID: "mockIdentifier",
				Identifiers: map[string]string{
					"email": "fakeEmail@fakecompany.org",
					"ip":    "4.4.4.4",
					"phone": "7777777777",
				},
				Parameters: map[string]interface{}{
					"RandomAttribute": []string{"value1", "value2"},
				},
				Version: "1.0",
				Signals: map[string]map[string]interface{}{
					"domain": {"Fraud": true, "Spam": true, "Suspect": true},
					"email":  {"Abuse": true, "Breach": true, "Deny List": true, "Fraud": true},
				},
				RequestId: "fakeID",
			},
			expectedError: nil,
			originalLdifWriter: &LdifWriter{
				writer: new(bytes.Buffer),
			},
		},
		{
			name: "Write: Bad Call",
			mockLdifWriter: &LdifWriter{
				writer: new(bytes.Buffer),
			},
			mockRecord: &Record{
				ID: "mockIdentifier",
				Identifiers: map[string]string{
					"email": "fakeEmail@fakecompany.org",
					"ip":    "4.4.4.4",
					"phone": "7777777777",
				},
				Parameters: map[string]interface{}{
					"RandomAttribute": []string{"value1", "value2"},
				},
				Version: "1.0",
				Signals: map[string]map[string]interface{}{
					"domain": {"Fraud": true, "Spam": true, "Suspect": true},
					"email":  {"Abuse": true, "Breach": true, "Deny List": true, "Fraud": true},
				},
				RequestId: "fakeID",
				Error:     "I am an error",
			},
			expectedError: nil,
			originalLdifWriter: &LdifWriter{
				writer: new(bytes.Buffer),
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := test.mockLdifWriter.Write(test.mockRecord)
			assert.Equal(t, test.expectedError, err)
			assert.NotEqual(t, test.mockLdifWriter, test.originalLdifWriter)
		})
	}
}
