// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"encoding/csv"
	"encoding/json"
	"io"

	"github.com/tidwall/gjson"
)

type CsvWriter struct {
	writer  *csv.Writer
	columns []string
}

func NewCsvWriter(writer io.Writer, columnPaths []string) (*CsvWriter, error) {
	csvWriter := csv.NewWriter(writer)
	err := csvWriter.Write(columnPaths)

	return &CsvWriter{
		writer:  csvWriter,
		columns: columnPaths,
	}, err
}

func (w *CsvWriter) Write(record *Record) error {
	// Convert record to a JSON object so we can path into it w/ gjson
	bs, err := json.Marshal(record)
	if err != nil {
		return err
	}

	row := make([]string, len(w.columns))

	for i, path := range w.columns {
		row[i] = gjson.GetBytes(bs, path).String()
	}

	err = w.writer.Write(row)
	if err != nil {
		return err
	}
	w.writer.Flush()
	return nil
}
