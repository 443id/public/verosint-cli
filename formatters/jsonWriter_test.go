// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"bytes"
	"encoding/json"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJsonWrite(t *testing.T) {
	tests := []struct {
		name           string
		w              JsonWriter
		recordInstance *Record
		expectedError  error
	}{
		{
			name: "Write: Good Call",
			w: JsonWriter{
				encoder: json.NewEncoder(&bytes.Buffer{}),
			},
			recordInstance: &Record{
				Identifiers: map[string]string{
					"email": "emailValue1",
					"phone": "phoneValue1",
					"ip":    "ipValue1",
				},
				Error: "",
			},
			expectedError: nil,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := test.w.Write(test.recordInstance)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestNewJsonWriter(t *testing.T) {
	testEncoder := json.NewEncoder(&bytes.Buffer{})
	testEncoder.SetIndent("", "  ")

	tests := []struct {
		name               string
		expectedJsonWriter JsonWriter
		ioW                io.Writer
		expectedError      error
	}{
		{
			name:          "NewJsonWriter: Good Call",
			ioW:           &bytes.Buffer{},
			expectedError: nil,
			expectedJsonWriter: JsonWriter{
				encoder: testEncoder,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			newWriter, err := NewJsonWriter(test.ioW)
			assert.Equal(t, &test.expectedJsonWriter, newWriter)
			assert.Equal(t, test.expectedError, err)
		})
	}
}
