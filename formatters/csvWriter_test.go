// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWrite(t *testing.T) {
	tests := []struct {
		name           string
		columns        []string
		recordInstance *Record
		expectedError  error
		expectedOut    string
	}{
		{
			name:    "Write: Score Result Contains Error",
			columns: []string{"error"},
			recordInstance: &Record{
				Identifiers: map[string]string{
					"email": "emailValue1",
					"phone": "phoneValue1",
					"ip":    "ipValue1",
				},
				Error: "fake error message",
			},
			expectedError: nil,
			expectedOut:   "error\nfake error message\n",
		},
		{
			name:    "Write: Good Call",
			columns: []string{"id", "identifiers.email", "parameters.blah", "outcomes"},
			recordInstance: &Record{
				Identifiers: map[string]string{
					"email": "emailValue1,",
				},
				Parameters: map[string]interface{}{
					"blah": "testBlah",
				},
				Outcomes: []string{"test outcome"},
			},
			expectedError: nil,
			expectedOut:   "id,identifiers.email,parameters.blah,outcomes\n,\"emailValue1,\",testBlah,\"[\"\"test outcome\"\"]\"\n",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actualOut := new(bytes.Buffer)
			csvWriter, err := NewCsvWriter(actualOut, test.columns)
			assert.Equal(t, test.expectedError, err)
			if err == nil {
				csvWriter.Write(test.recordInstance)
				assert.Equal(t, test.expectedOut, actualOut.String())
			}
		})
	}
}
