// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"bufio"
	"bytes"
	"io"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"github.com/go-ldap/ldif"
	"golang.org/x/exp/maps"
)

type LdifReader struct {
	scanner  bufio.Scanner
	ldif     *ldif.LDIF
	line     int
	mappings map[string]string
}

const (
	comment byte = '#'
	space   byte = ' '
)

//nolint:unparam // consistency with other readers
func NewLdifReader(reader io.Reader, mappings map[string]string) (*LdifReader, error) {
	l := &ldif.LDIF{}

	if len(mappings) == 0 {
		mappings = map[string]string{"email": "mail", "phone": "telephoneNumber"}
	}
	return &LdifReader{
		scanner:  *bufio.NewScanner(reader),
		ldif:     l,
		line:     0,
		mappings: mappings,
	}, nil
}

func handleDefault(firstChar byte, line string, isComment bool) (string, bool) {
	if firstChar == space {
		line = line[0:]
		return line, isComment
	}

	return line, false
}

func (r *LdifReader) Unmarshal(reader *bytes.Buffer, _ bool) (*Record, error) {
	err := ldif.Unmarshal(reader, r.ldif)
	if err != nil {
		return nil, &ldif.ParseError{Line: r.line, Message: err.Error()}
	}

	if len(r.ldif.Entries) == 0 {
		return nil, &ldif.ParseError{Line: r.line, Message: "no entries"}
	}

	if entry := r.ldif.Entries[0]; entry.Entry != nil {
		return r.toRecord(entry.Entry)
	}

	return nil, &ldif.ParseError{Line: r.line, Message: "null entry"}
}

func (r *LdifReader) Read() (*Record, error) {
	var line string
	lines := ""
	isComment := false
	r.ldif = &ldif.LDIF{}

	for {
		// eofStatus is true if not at end of file
		eofStatus := r.scanner.Scan()
		eof := !eofStatus

		if eof && len(lines) < 1 {
			return nil, io.EOF
		}

		line = r.scanner.Text()
		line = strings.TrimSpace(line)

		if eof {
			lines += line + "\n"
			byteSlice := []byte(lines)
			reader := bytes.NewBuffer(byteSlice)
			return r.Unmarshal(reader, eof)
		}

		if line == "" {
			break
		}

		if line[0] == comment {
			isComment = true
			r.line++
			continue
		}

		line, isComment = handleDefault(line[0], line, isComment)
		lines += line + "\n"
		r.line++
		continue
	}

	byteSlice := []byte(lines)
	reader := bytes.NewBuffer(byteSlice)

	return r.Unmarshal(reader, false)
}

func (r *LdifReader) toRecord(entry *ldap.Entry) (*Record, error) {
	identifiers := make(map[string]string)
	parameters := make(map[string]interface{})
	for _, attribute := range entry.Attributes {
		r.classifyAttribute(attribute, identifiers, parameters)
	}

	return &Record{
		ID:          entry.DN,
		Identifiers: identifiers,
		Parameters:  parameters,
	}, nil
}

// Classify an attribute in the entry as an identifier or a parameter
func (r *LdifReader) classifyAttribute(attribute *ldap.EntryAttribute, identifiers map[string]string, parameters map[string]interface{}) {
	isIdentifier := false
	for key, value := range r.mappings {
		// Attribute names are case insensitive in LDAP
		if strings.EqualFold(value, attribute.Name) {
			identifiers[key] = attribute.Values[0]
			isIdentifier = true
		}
	}
	if !isIdentifier {
		parameters[attribute.Name] = attribute.Values
	}
}

// Get the paths to the attributes/columns that will be read
func (r *LdifReader) Paths() []string {
	slice := make([]string, 0, len(r.mappings)+1)
	slice = appendPrefix(slice, "identifiers", maps.Keys(r.mappings)...)
	slice = append(slice, "parameters")
	return slice
}
