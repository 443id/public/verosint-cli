// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"strconv"

	"golang.org/x/exp/maps"
)

type CsvReader struct {
	reader           *csv.Reader
	indexes          map[string]int
	parameterIndexes map[string]int
	rowIndex         int
}

func NewCsvReader(reader io.Reader, mappings map[string]string) (*CsvReader, error) {
	csvReader := csv.NewReader(reader)
	headers, err := csvReader.Read()
	if err != nil {
		return nil, err
	}
	headers = trimInvisibleCharacters(headers)
	headers = trimWhitespace(headers)

	indexes := make(map[string]int)
	parameterIndexes := make(map[string]int)
	// First assume all columns are extra parameters. They will be removed below
	// once some headers are mapped to categories
	for i, header := range headers {
		parameterIndexes[header] = i
	}

	if len(mappings) == 0 {
		for i, header := range headers {
			if ValidIdentifiers[header] {
				indexes[header] = i
				delete(parameterIndexes, header)
			}
		}
	} else {
		for category, header := range mappings {
			i, err := getIndexForAttr(header, headers)
			if err != nil {
				return nil, err
			}
			indexes[category] = i
			delete(parameterIndexes, headers[i])
		}
	}

	return &CsvReader{
		reader:           csvReader,
		indexes:          indexes,
		parameterIndexes: parameterIndexes,
		rowIndex:         0,
	}, nil
}

func (r *CsvReader) Read() (*Record, error) {
	row, err := r.reader.Read()
	if err != nil {
		return nil, err
	}
	row = trimInvisibleCharacters(row)
	row = trimWhitespace(row)

	identifiers := make(map[string]string)
	parameters := make(map[string]interface{})
	for category, i := range r.indexes {
		identifiers[category] = row[i]
	}
	for name, i := range r.parameterIndexes {
		parameters[name] = row[i]
	}
	record := &Record{
		ID:          fmt.Sprintf("%v", r.rowIndex),
		Identifiers: identifiers,
		Parameters:  parameters,
	}
	r.rowIndex++
	return record, nil
}

// Get the paths to the attributes/columns that will be read
func (r *CsvReader) Paths() []string {
	slice := make([]string, 0, len(r.indexes)+len(r.parameterIndexes))
	slice = appendPrefix(slice, "identifiers", maps.Keys(r.indexes)...)
	slice = appendPrefix(slice, "parameters", maps.Keys(r.parameterIndexes)...)
	return slice
}

func getIndexForAttr(attr string, headers []string) (int, error) {
	if attr != "" {
		i, err := strconv.Atoi(attr)
		if err == nil {
			return i, nil
		} else {
			// need to find in headers
			for i, header := range headers {
				if header == attr {
					return i, nil
				}
			}
		}

		return -1, errors.New("No header with name " + attr)
	}
	return -1, nil
}
