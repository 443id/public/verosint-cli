// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"
	"github.com/go-ldap/ldif"
)

const (
	LDAP_TIMESTAMP_LAYOUT = "20060102150405Z0700"
)

type LdifWriter struct {
	writer io.Writer
}

//nolint:unparam // consistency with other formatters
func NewLdifWriter(writer io.Writer) (*LdifWriter, error) {
	return &LdifWriter{
		writer: writer,
	}, nil
}

func (w *LdifWriter) Write(record *Record) error {
	if record.Error != "" {
		_, err := fmt.Fprintf(w.writer, "#dn: %s\n#Error: %s\n\n", record.ID, record.Error)
		if err != nil {
			return err
		}
		return nil
	}

	now := time.Now()
	modify := ldap.NewModifyRequest(record.ID, []ldap.Control{})

	addObjectClass := true
	objectClassParam := record.Parameters["objectClass"]
	if objectClasses, ok := objectClassParam.([]string); ok {
		for _, objectClass := range objectClasses {
			if strings.ToLower(objectClass) == "verosint" {
				addObjectClass = false
			}
		}
	}

	var signalJson string
	if len(record.Signals) > 0 {
		signalBytes, err := json.Marshal(record.Signals)
		if err != nil {
			return err
		}
		signalJson = string(signalBytes)
	}

	if addObjectClass {
		modify.Add("objectClass", []string{"verosint"})
	}
	modifyMVAttribute("verosint-outcomes", record.Outcomes, record, modify)
	modifyMVAttribute("verosint-reasons", record.Reasons, record, modify)
	modifyAttribute("verosint-signals", signalJson, record, modify)
	modifyAttribute("verosint-requestId", record.RequestId, record, modify)
	modifyAttribute("verosint-timestamp", now.Format(LDAP_TIMESTAMP_LAYOUT), record, modify)
	err := ldif.Dump(w.writer, 0, modify)
	if err != nil {
		return err
	}

	return nil
}

func modifyAttribute(attributeName string, value any, record *Record, modify *ldap.ModifyRequest) {
	if value == nil || value == "" || value == 0 || value == 0.0 {
		if record.Parameters[attributeName] != nil {
			modify.Delete(attributeName, []string{})
		}
	} else {
		modify.Replace(attributeName, []string{fmt.Sprint(value)})
	}
}

func modifyMVAttribute(attributeName string, values []string, record *Record, modify *ldap.ModifyRequest) {
	if len(values) == 0 {
		if record.Parameters[attributeName] != nil {
			modify.Delete(attributeName, []string{})
		}
	} else {
		modify.Replace(attributeName, values)
	}
}
