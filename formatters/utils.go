// Copyright (c) "2025 Verosint, Inc"
package formatters

import (
	"strings"
	"unicode"
)

var ValidIdentifiers = map[string]bool{
	"accountId":   true,
	"deviceId":    true,
	"email":       true,
	"ip":          true,
	"paymentHash": true,
	"phone":       true,
	"sessionId":   true,
	"targetApp":   true,
	"userAgent":   true,
	"type":        true,
}

type Reader interface {
	Read() (*Record, error)
	Paths() []string
}

type Writer interface {
	Write(*Record) error
}

type Record struct {
	// Request data that was sent to 443ID
	ID          string                 `json:"id,omitempty"`
	Identifiers map[string]string      `json:"identifiers,omitempty"`
	Parameters  map[string]interface{} `json:"parameters,omitempty"`

	// Common response data that was returned from 443ID
	Version string `json:"version,omitempty"`
	// A map of categories to signals.
	Signals   map[string]map[string]interface{} `json:"signals,omitempty"`
	RequestId string                            `json:"requestId,omitempty"`
	Error     string                            `json:"error,omitempty"`

	// Rules evaluation response data that was returned from 443ID
	Outcomes []string `json:"outcomes,omitempty"`
	Reasons  []string `json:"reasons,omitempty"`
}

func trimInvisibleCharacters(strs []string) []string {
	for i, str := range strs {
		strs[i] = strings.Map(func(r rune) rune {
			if unicode.IsGraphic(r) {
				return r
			}
			return -1
		}, str)
	}
	return strs
}

func trimWhitespace(strs []string) []string {
	for i, str := range strs {
		strs[i] = strings.TrimSpace(str)
	}
	return strs
}

func appendPrefix(slice []string, path string, elems ...string) []string {
	for _, elem := range elems {
		slice = append(slice, path+"."+elem)
	}
	return slice
}
