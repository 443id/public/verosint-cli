# verosint

![verosint](https://gitlab.com/verosint/public/verosint/-/raw/main/assets/Verosint-logo-h-color.svg)

[![GoDoc](https://godoc.org/gitlab.com/verosint/public/verosint?status.svg)](https://godoc.org/gitlab.com/verosint/public/verosint)
[![Go Report Card][reportcard]](https://goreportcard.com/report/gitlab.com/verosint/public/verosint)
![Sonar Coverage](https://img.shields.io/sonar/coverage/443id_verosint/main?server=https%3A%2F%2Fsonarcloud.io&style=plastic)
[![Twitter Follow](https://img.shields.io/twitter/follow/verosint?style=plastic)](https://twitter.com/verosint)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=plastic&logo=linkedin)](https://www.linkedin.com/company/verosint)
[![Facebook](https://img.shields.io/badge/Facebook-1877F2?style=plastinc&logo=facebook)](https://www.facebook.com/Verosint/)

`verosint` is an open source tool that enables command line access to Verosint identity security endpoints.

## Installation

Use the `verosint` tool to:

* Send events to the SignalPrint endpoint to create a map of identities and access history.
* Evaluate workflows (formerly known as rule sets) one by one or in bulk.
* Generate an [LDAP Schema](https://www.rfc-editor.org/rfc/rfc4512#page-22)
file to store workflow evaluation atttributes in user entries.

Download the binary from:
[releases page](https://gitlab.com/verosint/public/verosint/-/releases).
Binaries for macOS, Windows, and Linux are available.

### Alternative Installation methods

#### Node Package Manager (NPM)

```shell
npm i -g @take2identity/verosint
```

Check permissions before installing the CLI tool. If installation
fails, run the command with "sudo" (MacOS/Linux), or run the command
inside a terminal using administrator privileges (Windows).

#### Using `go install`

```shell
go install gitlab.com/verosint/public/verosint@latest
```

#### Executing with a Docker container

Use the following command to run the CLI inside a Docker container
without having to install the tool on your system. This method
also enables running the CLI inside in a containerized
CI/CD pipeline.

```shell
docker run --rm registry.gitlab.com/verosint/public/verosint
```

Configuration for the CLI is stored in the `.verosint.yaml` file in the
home directory. To make the configuration accessible to the Docker
container, create a volume map for the configuration file. For example:

```shell
docker run --rm -v "$HOME/.verosint.yaml:/root/.verosint.yaml" registry.gitlab.com/verosint/public/verosint
```

### Updating the tool

Use `verosint update --check` to see if newer version is available.

:warning: Due to hosting changes, updating from versions prior to 0.2.32 is not supported and requires reinstallation.

## Usage

After installing the CLI, you can execute it using `verosint`.

Interacting with the Verosint APIs require an API key. You can pass the API
key using the `--apiKey` argument. It is recommended to store the API
key inside a configuration file or an environment variable to prevent
exposure.

You can get command help using the `--help` argument:

```shell
verosint --help
```

Note that each subcommand has its own specific help. For example, to
print the help for the `evaluate rules-batch` subcommand, run:

```shell
verosint evaluate rules-batch --help
```

### Configuration File

The tool can read settings from a configuration file in a
[YAML](https://yaml.org/) format.
For example, you can store the API key as well as workflows inside the file.

By default, the tool looks for the configuration file `.verosint.yaml` in
your home directory. You can override this setting with the
`--configFile` flag.

An example configuration file:

```yaml
apiKey: <API key goes here>
rules:
    - name: IP - Tor/Bot/VPN
      outcomes:
        - DENY
      query: 'signals.ip.bot || signals.ip.tor || signals.ip.vpn'
      reason: This IP address is a known bot, active Tor node, or a VPN
```

#### API key as an Environment Variable

You can also set the `VEROSINT_APIKEY` environment variable to hold the value
of the API key:

```shell
export VEROSINT_APIKEY="API key goes here"
verosint evaluate evaluate rule phone:15123944240 --ruleSetUuid 4f5ab21b-984c-455e-b889-b6b0272a4567
```

#### Account IDs

When using commands involving an account ID you should
wrap the identifier in double quotes.

### Evaluating Workflows

#### Single Set of Identifiers

The CLI accepts the following identifiers.

| Name | Description |
| ---- | ----------- |
| accountId | Unique account identifier |
| deviceId | Device identifier string |
| email | Email address |
| ip | IP address (either IPv4 or IPv6) |
| paymentHash | Payment hash string |
| phone | Phone number in the [international phone number format](https://en.wikipedia.org/wiki/E.164) |
| sessionId | Session identifier string |
| targetApp | Application name string |
| userAgent | User agent string |

When providing a set of identifiers, the API expects one or more
identifiers (IP address, email, or phone number) and only one value per
identifier.

IPv6 addresses may compress zeros for a shorter form and use
representations as described in [RFC 5952](https://www.rfc-editor.org/rfc/rfc5952).

Responses are provided in the JSON format on the standard output.

##### Examples

###### 2. Evaluate an IPv6 address, a phone number and an email address for a Workflow

```shell
verosint evalute rule ip:2607:fb91:1296:c7dc:a0c4:25a9:ac7a:4384 email:user@example.com phone:15123944240 --ruleSetUuid 4f5ab21b-984c-455e-b889-b6b0272a4567

```

###### 3. Evaluate an IPv4 for address against a workflow already defined in the configuration

To obtain the UUID of the workflow, visit the
[Workflows](https://app.verosint.com/workflows) configuration and copy the UUID of
the workflow you would like to evaluate.

![Copy Workflow UUID](https://gitlab.com/verosint/public/verosint/-/raw/main/assets/copyworkflowuuid.png)

```shell
verosint evalute rule ip:104.255.6.45 --ruleSetUuid 4f5ab21b-984c-455e-b889-b6b0272a4567
```

###### 4. Evaluate an Email address against a workflow provided using a local file

You can export a workflow defined in the [Workflows](https://app.verosint.com/workflows)
configuration into a local file as shown below.

![Export Workflow](https://gitlab.com/verosint/public/verosint/-/raw/main/assets/exportworkflow.png)

This example evaluates the `babs@jensen.com` email address against the rule defined
in the `mfaworkflow.json` file.

```shell
verosint evaluate rule email:babs@jensen.com --ruleSetFile mfaworkflow.json
```

##### 5. Check if an IP address is within 100 kilometers of Austin, TX using a local workflow

You can place a workflow inside a local configuration file. The following is
an example file that uses the `isWithin` function, which enables you to
create a workflow for [geo-fencing](https://en.wikipedia.org/wiki/Geo-fence)
purposes:

```yaml
apiKey: <API key goes here>
rules:
    - name: IP not in Austin
      outcomes:
        - DENY
      query: '!signals.ip.geo.isWithin(30.3079827, -97.895826, 100)'
      reason: This IP is not within 100 kilometers of Austin, Texas
```

The following example runs the same workflow saved in the default
configuration file against the `104.16.44.99` IP address:

```shell
verosint evaluate rules ip:104.16.44.99
```

#### Batch Evaluation

Use input files to evaluate multiple sets of identifiers against workflows.
Batch commands can use input and output files in CSV or
LDIF format, and can produce a report file formatted as JSON.

Processing time may take much longer with larger files, than for a
single set of identifiers.

##### Batch Evaluation Examples

###### 2. Executing Batch Workflow Evaluation with column index and LDIF output

If using an input file that has multiple identifiers, you can provide a
column index (where 0 refers to the first column) to indicate where the
value of a particular identifier is present. This content is saved in
`mapped.csv`.

```csv
ipaddress,mail,telephone
104.16.44.99,babs@jensen.com,15123944240
```

Using the workflows present in the default configuration file, the
following command generates LDIF-formatted output and JSON report
files:

```shell
verosint evaluate rules-batch ip:0 email:1 phone:2 \
  --inputFile mapped.csv \
  --outputFile output.ldif --outputType ldif \
  --reportFile report.json
```

#### Submitting SignalPrint Events

You can submit events to the SignalPrint endpoint using the
`signalprint send-events` subcommand. This subcommand works like the
batch commands for workflows. An IP address, a user agent, and an event
type are required for each event. You can optionally submit an
event timestamp, an account identifier, an email address, and
a phone number as well.

The timestamp is expected to be in [RFC3339](https://www.rfc-editor.org/rfc/rfc3339) or [UNIX](https://www.unixtimestamp.com/) format.

For example:

* RFC3339: `2019-01-22T03:56:17+03:30`
* UNIX: `1548116777`

The type must be one of:

* `ACCOUNT_RECOVERY_FAILED`
* `ACCOUNT_RECOVERY_SUCCESS`
* `CHANGE_EMAIL_FAILED`
* `CHANGE_EMAIL_SUCCESS`
* `CHANGE_PASSWORD_FAILED`
* `CHANGE_PASSWORD_SUCCESS`
* `CHANGE_PAYMENT_FAILED`
* `CHANGE_PAYMENT_SUCCESS`
* `CHANGE_PHONE_FAILED`
* `CHANGE_PHONE_SUCCESS`
* `CHANGE_USERNAME_FAILED`
* `CHANGE_USERNAME_SUCCESS`
* `EMAIL_SUCCESS`
* `LOGIN_FAILED`
* `LOGIN_SUCCESS`
* `LOGOUT_FAILED`
* `LOGOUT_SUCCESS`
* `MFA_DEVICE_UNENROLLED`
* `MFA_ENROLLMENT_FAILED`
* `MFA_ENROLLMENT_SUCCESS`
* `MFA_FAILED`
* `MFA_STARTED`
* `MFA_SUCCESS`
* `PAYMENT_ADDED_FAILED`
* `PAYMENT_ADDED_SUCCESS`
* `PAYMENT_FAILED`
* `PAYMENT_REMOVED_FAILED`
* `PAYMENT_REMOVED_SUCCESS`
* `PAYMENT_SUCCESS`
* `PUSH_NOTIFICATION_FAILED`
* `PUSH_NOTIFICATION_SUCCESS`
* `SIGNUP_FAILED`
* `SIGNUP_SUCCESS`
* `SMS_FAILED`
* `SMS_SUCCESS`
* `TOKEN_ISSUED_SUCCESS`
* `VERIFICATION_SUCCESS`

##### Examples for Signal Print

Using the following event information saved in `events.csv`,

```csv
timestamp,ip,accountId,email,phone,userAgent,type,deviceId
2019-01-22T03:56:17-05:00,104.16.44.99,babs_jensen,babs@jensen.com,15123944240,"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36",LOGIN_SUCCESS,device-abc-1234
```

you can send the event to SignalPrint with:

```csv
verosint signalprint send-events --inputFile events.csv
```

### Generating LDAP Schema

This command prints an LDAP schema that allows you to enrich existing
user entries in your directory with workflow evaluation data from
the API. The enterprise number (59592) in the object identifiers (OIDs)
is registered with the
[Internet Assigned Numbers Authority](https://www.iana.org/assignments/enterprise-numbers/).
This guarantees that the schema will not collide with your existing
schema elements.

Note that generating the LDAP schema does *not* require an API key. To
generate a schema, run the following command:

```shell
verosint generate schema
```

## Development

## Prerequisites

### Go

Install the 1.23 version of [Go](https://golang.dev).

### Task

The [task](https://taskfile.dev/installation/) utility is required to
execute the various build related tasks.

### Testing

Unit tests can be executed using the `task test:unit` command. Our goal
is to maintain a minimum of 70% coverage.

## Issues

Report issues at [Verosint Support](mailto:support@verosint.com)

[reportcard]: https://goreportcard.com/badge/gitlab.com/verosint/public/verosint
